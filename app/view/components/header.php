<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Controle de Vendas</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarColor02">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item me-5">
                        <a class="nav-link " href="/home">
                            <i class="fi fi-sr-home"></i>
                            Home
                        </a>
                    </li>
                    <li class="nav-item me-5">
                        <a class="nav-link" href="/produtos">
                            <i class="fi fi-sr-shopping-cart-add"></i>
                            Produtos
                        </a>
                    </li>
                    <li class="nav-item me-5">
                        <a class="nav-link" href="/clientes">
                            <i class="fi fi-sr-users"></i>
                            Clientes</a>
                    </li>
                    <li class="nav-item me-5">
                        <a class="nav-link" href="/notas">
                            <i class="fi fi-sr-money-check-edit"></i>
                            Notas</a>
                    </li>

                </ul>
                <a class="btn btn-primary my-2 my-sm-0" type="submit" id="logout">Sair
                    <span id="spinner"></span>
                </a>
            </div>
        </div>
    </nav>
</header>
<script src="/public/js/header.js"></script>
