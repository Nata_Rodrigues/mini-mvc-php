<html>
<header>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{BASE}}public/css/login.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-solid-rounded/css/uicons-solid-rounded.css'>

</header>

<body>
    <div class="box-parent-login h-100">
        <div class="box-login">
            <h1 class="ls-login-logo">{{APP_NAME}}</h1>
            <form class="form-group">
                <fieldset>
                    <div class="alert alert-danger show" role="alert" id="alert1">
                        Digite o e-mail e a senha para continuar.
                    </div>
                    <div class="alert alert-danger show" role="alert" id="alert2"></div>
                    <div class="form-group ls-login-user">
                        <label for="userLogin">Usuário</label>
                        <input class="form-control ls-login-bg-user input-lg" id="user" type="text" aria-label="Usuário" placeholder="Usuário">
                    </div>
                    <div class="form-group ls-login-password">
                        <label for="userPassword">Senha</label>
                        <input class="form-control input-lg" id="password" type="password" aria-label="Senha" placeholder="Senha">
                    </div>
                </fieldset>
                <a class="btn btn-outline-primary" id="login">Entrar
                    <span id="spinner"></span>

                </a>


            </form>
        </div>
    </div>
    <script src="{{BASE}}public/js/login.js"></script>
</body>

</html>