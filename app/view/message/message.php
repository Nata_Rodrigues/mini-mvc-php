{% extends 'components/body.php' %}
{% block title %}
Home - Mini Framework
{% endblock %}
{% block body %}
<div class="max-width center-screen main-body padding">
<div class="card border-danger mb-3" >
  <div class="card-header">{{title}}</div>
  <div class="card-body">
    <h4 class="card-title"></h4>
    <p class="card-text">{{message}}</p>
  </div>
</div>
</div>
{% endblock %}