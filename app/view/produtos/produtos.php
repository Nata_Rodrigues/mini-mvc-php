{% extends 'components/body.php' %}
{% block title %}
Produtos - {{APP_NAME}}
{% endblock %}
{% block body %}
<link rel="stylesheet" href="{{BASE}}/public/css/produtos.css" />
<div class="max-width center-screen main-body padding">


    <div class="container">
        <div class="header">
            <h1>
                Produtos
            </h1>
        </div>
        <hr class="border border-secondary border-1 opacity-50 m-auto mb-2">
        <div class="bt-icon">
            <a class="btn btn-success btn-sm" id="btn_adicionar"><i class="fi fi-sr-plus"></i>Adicionar</a>
            <div class="d-flex align-items-center gap-1">
                <label>Pesquisar: </label>
                <input class="form-control form-control-sm" id="search" type="text">
            </div>
        </div>
        <table class="table table-striped" id="mytable">
            <thead class="table-dark">
                <tr>
                    <th style="text-align: center;" scope="col">#cod</th>
                    <th style="text-align: center;" scope="col">Descrição</th>
                    <th style="text-align: center;" scope="col">Preço</th>
                    <th style="text-align: center;" scope="col">Estoque</th>
                    <td style="text-align: right;"></td>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5" style='text-align: center;'>
                        <span id="spinner_table"></span>
                    </td>
                </tr>
            </tbody>
            <div>
        </table>
        <div style="justify-content: space-between; display: flex;">
            <div>
                <label>Mostrando: </label>
                <select name="" id="perPage" class=" perPage">

                    <option value="15">15</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <label> registros por página</label>
            </div>
            <div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm" style="justify-content: end;">
                        <li class="page-item "><a class="page-link first" href="#">Primeiro</a></li>
                        <li class="page-item">
                            <a class="page-link prev" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item numbers d-flex"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link next" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li class="page-item "><a class="page-link last" href="#">Último</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="titleModalLabel">Adicionar Produto</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="modal_body">
                    <form>
                        <fieldset>
                            <input type="hidden" id="id_produto" />
                            <div class="form-group">
                                <label class="form-label mt-4" style="color: #0095da; ">Descrição</label>
                                <input type="text" class="form-control" id="descricao_produto" placeholder="">
                                <small id="inputAlert" class="form-text text-muted"></small>
                            </div>
                            <div class="form-group">
                                <label class="form-label mt-4" style="color: #0095da; " >Valor</label>
                                <input type="text" class="form-control" id="valor_produto" placeholder="R$">
                                <small id="inputAlert" class="form-text text-muted valor_produto"></small>

                            </div>
                            <div class="form-group">
                                <label class="form-label mt-4" style="color: #0095da; " >Estoque</label>
                                <input type="number" class="form-control" id="estoque_produto" value="0">
                                <small id="inputAlert" class="form-text text-muted"></small>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="salvar_produto">Salvar
                        <span id="spinner_save"></span>

                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<script src="{{BASE}}public/js/produtos.js"></script>
{% endblock %}