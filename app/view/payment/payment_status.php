<html>
<header>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="/public/css/payment.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-solid-rounded/css/uicons-solid-rounded.css'>
</header>

<body>
    <div class="box-parent-login h-100">
        <div class="box-login">
            <div class="d-flex justify-content-center">
                <h1 class="ls-login-logo">{{APP_NAME}}</h1>
            </div>
            <div class="container">
                <input type="hidden" id="public_key" value="{{ key }}" />
                <input type="hidden" id="id_nota" value="{{nota.id}}" />

                <fieldset>
                    <div class="alert alert-danger show" role="alert" id="alert2"></div>
                    <div class="d-flex justify-content-between ">
                        <label>Nota Nº: {{nota.id}}</label>
                        <label>Data {{nota.data}}</label>

                    </div>
                    <label>Cliente: {{nota.cliente}}</label>
                    <table class="table mt-5" id="mytable">
                        <thead class="table">
                            <tr>
                                <th style="text-align: center;" scope="col">#cod</th>
                                <th style="text-align: center;" scope="col">Produto</th>
                                <th style="text-align: center;" scope="col">Qnt.</th>
                                <th style="text-align: center;" scope="col">Valor</th>

                            </tr>
                        </thead>
                        <tbody>
                            {% for produto in nota.produtos %}
                            <tr>
                                <th style="text-align: center;" scope="col">{{ produto.id }}</th>
                                <th style="text-align: center;" scope="col">{{ produto.descricao }}</th>
                                <th style="text-align: center;" scope="col">{{ produto.quantidade }}</th>
                                <th style="text-align: center;" scope="col">{{ produto.valor }}</th>

                            </tr>
                            {% endfor %}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align: left;" colspan="3" scope="col">Total</th>
                                <th style="text-align: center;" scope="col">{{ nota.valorTotal }}</th>
                            </tr>
                        </tfoot>
                    </table>

                </fieldset>
                <div class="d-flex justify-content-between align-items-center">
                    <div class='badge  text-dark' style='background-color:{{nota.cor_status}};'>{{nota.status}}</div>
                    <span>Pagamento: {{nota.dt_pagamento}}</span>
                </div>
                <div class="d-flex  justify-content-end">
                    <a class="btn btn-info mt-4" type="reset" id="comprovante">comprovante</a>

                </div>
                </d>
            </div>
        </div>
        <script>
            $(document).ready(function () {
            $('#comprovante').on('click',  function() {

                var idNota =$('#id_nota').val();
                var url_atual = window.location.origin;
                console.log(url_atual);
                window.open(url_atual + "/notas/comprovante?ID=" + idNota, "_blank");
            });

        });
        </script>
</body>

</html>