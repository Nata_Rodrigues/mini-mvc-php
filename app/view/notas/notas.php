{% extends 'components/body.php' %}
{% block title %}
Clientes - {{APP_NAME}}
{% endblock %}
{% block body %}
<link rel="stylesheet" href="{{BASE}}/public/css/notas.css" />
<div class="max-width center-screen main-body padding">


    <div class="container">
        <div class="header">
            <h1>
                Notas
            </h1>

        </div>
        <hr class="border border-secondary border-1 opacity-50 m-auto mb-2">
        <div class="pesquisar">
            <div class="info_cliente">
                <label class="form-label mb-0" style="color: #0095da; ">Tipo</label>
                <div class="input-group input-group-sm">

                    <select name="tp_pesquisa" id="tp_pesquisa" class="btn btn-outline-secondary select ">
                        <option value="0">Selecione</option>
                        <option value="cliente">Cliente</option>
                        <option value="nota">Nota</option>
                    </select>
                    <input type="text" class="form-control " name="tp_pesquisa_input" id="tp_pesquisa_input" aria-label="Text input with dropdown button">
                </div>
            </div>
            <div>
                <label class="form-label mb-0" style="color: #0095da; ">Período</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">De:</span>
                    </div>
                    <input type="date" class="form-control" id="data_inicial" name="data_inicial" aria-describedby="basic-addon1">
                </div>
            </div>
            <div>
                <label class="form-label mb-0" style="color: #0095da; "></label>
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Até:</span>
                    </div>
                    <input type="date" class="form-control " id="data_final" name="data_final" aria-describedby="basic-addon1">
                </div>
            </div>
            <div>
                <label class="form-label mb-0" style="color: #0095da; "></label>
                <div class="bt-icon">
                    <a class="btn btn-primary " id="btn_pesquisa"><i class="fi fi-rr-search"></i>Pesquisa
                        <span id="spinner_pesquisa"></span>
                    </a>
                </div>
            </div>

        </div>
        <div class="bt-icon">
            <a class="btn btn-success btn-sm" id="btn_adicionar"><i class="fi fi-sr-plus"></i>Adicionar</a>
            <div class="d-flex align-items-center gap-1">
                <label>Pesquisar: </label>
                <input class="form-control form-control-sm" id="search" type="text">
            </div>
        </div>
        <table class="table table-striped" id="mytable">
            <thead class="table-dark">
                <tr>
                    <th style="text-align: center;" scope="col">#cod</th>
                    <th style="text-align: center;" scope="col">Data</th>
                    <th style="text-align: center;" scope="col">Cliente</th>
                    <th style="text-align: center;" scope="col">Status</th>
                    <th style="text-align: center;" scope="col">Valor</th>
                    <td style="text-align: right;"></td>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5" style='text-align: center;'>
                        <span id="spinner_table"></span>
                    </td>
                </tr>
            </tbody>
            <div>
        </table>
        <div style="justify-content: space-between; display: flex;">
            <div>
                <label>Mostrando: </label>
                <select name="" id="perPage" class=" perPage">

                    <option value="15">15</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <label> registros por página</label>
            </div>
            <div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm" style="justify-content: end;">
                        <li class="page-item "><a class="page-link first" href="#">Primeiro</a></li>
                        <li class="page-item">
                            <a class="page-link prev" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item numbers d-flex"><a class="page-link" href="#">1</a></li>
                        <li class="page-item">
                            <a class="page-link next" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li class="page-item "><a class="page-link last" href="#">Último</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- Modal Notas -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="titleModalLabel"></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="modal_body">
                    <input type="hidden" id="id_nota" />
                    <div class="container-fluid">
                        <div class="modal_cliente">
                            <label class="form-label mb-0" style="color: #0095da; ">Cliente</label>
                            <select class="form-select select2" aria-label="Default select example" id="select_cliente">
                                <option value="-1" selected>Selecione...</option>
                                {% for cliente in clientes %}
                                <option value="{{ cliente.id }}">{{ cliente.nome }}</option>
                                {% endfor %}
                            </select>
                            <small id="inputAlert" class="form-text text-muted" style="color:red!important"></small>
                        </div>
                        <div class="modal_produtos mb-4">
                            <div class=" form-group select_produto">
                                <label class="form-label mt-4" style="color: #0095da; ">Produto</label>
                                <select class="form-select " aria-label="Default select example" id="select_produto">
                                    <option value="-1" selected>Selecione...</option>
                                    {% for produto in produtos %}
                                    <option value="{{ produto.id }}">{{ produto.descricao }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                            <div class=" form-group">
                                <label class="form-label mt-4" style="color: #0095da; ">Quantidade</label>
                                <input type="number" class="form-control" id="quantidade_produto" placeholder="">
                            </div>
                            <div class=" form-group">
                                <label class="form-label mt-4" style="color: #0095da; ">Valor</label>
                                <input type="tel" class="form-control valor_produto" id="valor_produto">
                            </div>
                            <div class=" form-group">
                                <label class="form-label mt-4">&nbsp</label>
                                <button type="button" class="btn btn-success" id="btn_adicionar_produto">Adicionar</button>

                            </div>
                        </div>
                        <table class="table table-striped" id="produtos_notas">
                            <thead class="table-dark">
                                <tr>
                                    <th style="text-align: center;" scope="col">#cod</th>
                                    <th style="text-align: center;" scope="col">Produto</th>
                                    <th style="text-align: center;" scope="col">Quantidade</th>
                                    <th style="text-align: center;" scope="col">Valor</th>
                                    <td style="text-align: right;"></td>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" style='text-align: center;'>
                                        <span id="spinner_table"></span>
                                    </td>
                                </tr>
                            </tbody>
                            <div>
                        </table>
                        <div>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination pagination-sm" style="justify-content: end;">
                                    <li class="page-item "><a class="page-link first-produto" href="#">Primeiro</a></li>
                                    <li class="page-item">
                                        <a class="page-link prev-produto" href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li class="page-item numbers-produto d-flex"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item">
                                        <a class="page-link next-produto" href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                    <li class="page-item "><a class="page-link last-produto" href="#">Último</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="modal_info">
                            <div class="total-nota">
                                <div class="form-group ">
                                    <label class="form-label " style="color: #0095da; ">Total</label>
                                    <label class="input-group " style="color: black; font-size:29.5px;"><span class="badge bg-secondary" id="total_nota"></span></label>
                                </div>
                            </div>
                            <div class="pagamento-status">
                                <div class="form-group">
                                    <label class="form-label mt-4" style="color: #0095da; ">Pagamento</label>
                                    <input type="date" class="form-control" id="dt_pagamento" placeholder="">
                                </div>
                                <div class=" form-group ">
                                    <label class="form-label mt-4" style="color: #0095da; ">Status</label>
                                    <select class="form-select " aria-label="Default select example" id="status_nota">
                                        <option value="-1" selected>Selecione...</option>
                                        {% for row in status %}
                                        <option value="{{ row.id }}">{{ row.descricao }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="salvar_nota">Salvar
                        <span id="spinner_save"></span>

                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Cobranca-->
    <div class="modal fade" id="cobrancaModal" tabindex="-1" aria-labelledby="cobrancaModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable ">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="titleModalLabel">Enviar Cobrança</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <input type="hidden" id="id_nota_cobranca" />
                <div class="modal-body" id="cobrancaModal_body" tyle="overflow:auto">
                </div>
                <div class="modal-footer">
                    <div class="bt-icon ">

                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    </div>
                    <div class="bt-icon ">
                        <a type="button" class="btn gap-1" style='background-color: #a807f2; color:#ffffff' id="enviar_email_coanca">
                            <div >
                                <span id="spinner_cobranca"></span>
                            </div>
                            <span>Enviar</span>
                            <i class="fi fi-sr-envelope"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>

</div>
<script src="{{BASE}}/public/js/notas.js"></script>
{% endblock %}