<?php

namespace app\controller;
use app\classes\Input;
use app\core\Controller;
use app\model\Usuarios;

class  UsuariosController extends Controller
{
    public $logado;
    public function __construct()
    {
        $this->logado = $this->allSessionPlataform();  // a this->logado ser� como uma session ativa

    }

    public function index()
    {
    }
    public function insert()
    {
       

        if (!Input::post('email') or !Input::post('password') or !Input::post('nome')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $email =  Input::post('email');
        $password =  Input::post('password');
        $nome =  Input::post('nome');

        $usuario = new Usuarios();
        if($usuario->findBy('email',$email)){
            
            returnJson([
                'message' => "Email já cadstrado na base de dados.",
                'status' => false
            ]);

        }
        if($id = $usuario->insert($nome, $email,Encript($password))){
            
            returnJson([
                'message' => "Usuario cadastrado com Sucesso",
                'status' => true,
                'id_usuario'=> $id
            ]);
        }else{
            returnJson([
                'message' => "Erro ao cadastrar Usuario",
                'status' => false
            ]);
        }


    }
    public function update()
    {
    }
    public function delete()
    {
    }
}
