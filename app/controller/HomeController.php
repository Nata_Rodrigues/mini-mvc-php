<?php

namespace app\controller;

use app\classes\Input;
use app\core\Controller;

class  HomeController extends Controller
{
    public $logado;

    public function __construct()
    {
        $this->logado = $this->allSessionPlataform();  

    }

    public function index()
    {
        $this->load('home/main');
    }
}
