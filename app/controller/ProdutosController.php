<?php

namespace app\controller;

use app\model\Produtos;
use app\classes\Input;
use app\core\Controller;

class  ProdutosController extends Controller
{

    public $logado;
    public $produtos;

    public function __construct()
    {
        $this->logado = $this->allSessionPlataform();
        $this->produtos = new Produtos;
    }

    public function index()
    {

        $this->load('produtos/produtos');
    }

    public function getAll()
    {

        returnJson($this->produtos->getAll());
    }

    public function getId()
    {

        if (!Input::get('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::get('ID');
        returnJson($this->produtos->getId($id));
    }

    public function insert()
    {

        if ($this->validateInsert()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $descricao =  Input::post('DESCRICAO');
        $valor =  gravaValor(Input::post('VALOR'));
        $estoque =  Input::post('ESTOQUE');

        if ($this->produtos->insert($descricao, $valor, $estoque)) {

            returnJson([
                'message' => "Produto adicionado com sucesso!",
                'status' => true,
            ]);
        } else {
            returnJson([
                'message' => "Erro ao adicionar produto, tente novamente.",
                'status' => false
            ]);
        }
    }

    public function delete()
    {
        if (!Input::delete('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::delete('ID');

        if ($this->produtos->delete($id)) {

            returnJson([
                'message' => "Produto excluido com sucesso!",
                'status' => true,
            ]);
        } else {
            returnJson([
                'message' => "Erro ao excluir produto, tente novamente.",
                'status' => false
            ]);
        }
    }

    public function salvar()
    {

        if (!$this->validateUpdate()) {
            $this->update();
        } elseif (!$this->validateInsert()) {
            $this->insert();
        } else {
            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }
    }

    public function update()
    {
        if ($this->validateUpdate()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::post('ID');
        if ($this->produtos->getId($id)) {
            $descricao =  Input::post('DESCRICAO');
            $valor =  gravaValor(Input::post('VALOR'));
            $estoque =  Input::post('ESTOQUE');

            if ($this->produtos->update($id, $descricao, $valor, $estoque)) {

                returnJson([
                    'message' => "Produto atualizado com sucesso!",
                    'status' => true,
                ]);
            } else {
                returnJson([
                    'message' => "Erro ao atualizado produto, tente novamente.",
                    'status' => false
                ]);
            }
        } else {
            returnJson([
                'message' => "Erro ao atualizado produto, tente novamente.",
                'status' => false
            ]);
        }
    }

    private function validateInsert()
    {
        if (!Input::post('DESCRICAO') or (Input::post('VALOR') == '' or  Input::post('VALOR') < 0) or (Input::post('ESTOQUE') == '' or Input::post('ESTOQUE') < 0)) {

            return true;
        } else {
            return false;
        }
    }

    private function validateUpdate()
    {
        if (!Input::post('DESCRICAO') or (Input::post('VALOR') == '' or  Input::post('VALOR') < 0) or (Input::post('ESTOQUE') == '' or Input::post('ESTOQUE') < 0) or (Input::post('ID') == '' or Input::post('ID') <= 0)) {

            return true;
        } else {
            return false;
        }
    }
}
