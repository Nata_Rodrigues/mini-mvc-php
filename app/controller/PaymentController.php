<?php

namespace app\controller;

use app\classes\Email;
use app\classes\Input;
use app\core\Controller;
use app\model\Notas;
use app\model\ItemNotas;
use app\model\Clientes;
use app\model\Produtos;
use app\model\Status;
use app\model\Payment;
use app\model\Notification;
use MercadoPago;

class  PaymentController extends Controller
{

    public $notas;
    public $itemNotas;
    public $clientes;
    public $produtos;
    public $status;
    public $email;
    public $payment;
    public $notification;
    public function __construct()
    {
        $this->notas = new Notas;
        $this->itemNotas = new ItemNotas;
        $this->clientes = new Clientes;
        $this->produtos = new Produtos;
        $this->status = new Status;
        $this->payment = new Payment;
        $this->notification = new Notification;
    }

    public function index()
    {
        try {
            if (!Input::get('TOKEN')) {

                returnJson([
                    'message' => "Dados Invalidos",
                    'status' => false
                ]);
            }

            $token = Input::get('TOKEN');
            $token = base64_decode($token);
            $dadosTokem = sslDecrypt($token);
            $dadosTokem = json_decode($dadosTokem, true);
            $id = $dadosTokem['ID'];
            $key = $dadosTokem['KEY'];
            if ($key != PAYMENT_SECRET_KEY) {
                returnJson([
                    'message' => "Token inválido",
                    'status' => false
                ]);
            }
            $nota = $this->notas->getId($id);
            if (!$nota) {
                returnJson([
                    'message' => "Nota não encontrada na base de dados",
                    'status' => false
                ]);
            }

            if ($nota['id_status'] == 2) {
                $this->payment_status($id);
                die;
            }

            $totalNota = $this->itemNotas->getValorTotal($id);
            $nota['valorTotal'] = valorFormatado($totalNota['valorTotal']);
            $cliente = $this->clientes->getId($nota['id_cliente']);
            $nota['cliente'] = $cliente['nome'];
            $nota['id_cliente'] = $cliente['id'];
            $status = $this->status->getId($nota['id_status']);
            $nota['id_status'] = $status['id'];
            $nota['status'] = $status['descricao'];
            $nota['cor_status'] = $status['cor'];
            $produtos = $this->itemNotas->findBy('id_nota', $id);
            foreach ($produtos as $index => $produto) {
                $dadosProduto = $this->produtos->getId($produto['id_produto']);
                $produtos[$index]['descricao'] = $dadosProduto['descricao'];
                $valor = $this->itemNotas->getProdutos_Nota($id, $produto['id_produto']);
                $produtos[$index]['valor'] = valorFormatado($valor['valor']);
            }
            $nota['produtos'] = $produtos;
            $nota['data'] = mostraData($nota['creat_at']);




            if (MERCADOPAGO_AMBIENTE == 'sandbox') {
                $key = MERCADOPAGO_PUBLIC_KEY;
            } else {
                $key = MERCADOPAGO_PUBLIC_KEY_PRODUCAO;
            }
            $this->load('payment/payment', [
                'key' => $key,
                'nota' => $nota,
            ]);
        } catch (\Exception $e) {
        }
    }
    public function paymentSave()
    {


        if (MERCADOPAGO_AMBIENTE == 'sandbox') {
            \MercadoPago\SDK::setAccessToken(MERCADOPAGO_ACCESS_TOKEN);
        } else {
            \MercadoPago\SDK::setAccessToken(MERCADOPAGO_ACCESS_TOKEN_PRODUCAO);
        }

        $idNota =  Input::post('ID');

        $totalNota = $this->itemNotas->getValorTotal($idNota);
        $preference = new MercadoPago\Preference();
        $item = new MercadoPago\Item();
        $item->title = 'Nota Nº ' . $idNota;
        $item->quantity = 1;
        $item->unit_price = $totalNota['valorTotal'];
        $preference->items = array($item);

        $preference->back_urls = array(
            "success" => BASE_URL . "payment/success",
            "failure" => BASE_URL . "payment/failure",
            "pending" => BASE_URL . "payment/pending"
        );
        $preference->auto_return = "approved";
        $preference->external_reference = $idNota;
        $preference->notification_url = BASE_URL . "/payment/notificaction";
        $preference->save();

        $response = array(
            'id' => $preference->id,
        );
        echo json_encode($response);
    }

    public function paymentSuccess()
    {

        $data =  Input::get();
        $return = $this->payment->insert($data);
        $idNota =  $data['external_reference'];
        $this->payment_status($idNota);
    }
    public function paymentFailure()
    {

        $data =  Input::get();
        $return = $this->payment->insert($data);
        $idNota =  $data['external_reference'];
        $this->payment_status($idNota);
    }

    public function paymentPending()
    {
        $data =  Input::get();
        $return = $this->payment->insert($data);
        $idNota =  $data['external_reference'];
        $this->payment_status($idNota);
    }

    public function payment_status($idNota)
    {

        $nota = $this->notas->getId($idNota);
        $nota['dt_pagamento'] = mostraData($nota['dt_pagamento']);
        $totalNota = $this->itemNotas->getValorTotal($idNota);
        $nota['valorTotal'] = valorFormatado($totalNota['valorTotal']);
        $cliente = $this->clientes->getId($nota['id_cliente']);
        $nota['cliente'] = $cliente['nome'];
        $nota['id_cliente'] = $cliente['id'];
        $status = $this->status->getId($nota['id_status']);
        $nota['id_status'] = $status['id'];
        $nota['status'] = $status['descricao'];
        $nota['cor_status'] = $status['cor'];
        $produtos = $this->itemNotas->findBy('id_nota', $idNota);
        foreach ($produtos as $index => $produto) {
            $dadosProduto = $this->produtos->getId($produto['id_produto']);
            $produtos[$index]['descricao'] = $dadosProduto['descricao'];
            $valor = $this->itemNotas->getProdutos_Nota($idNota, $produto['id_produto']);
            $produtos[$index]['valor'] = valorFormatado($valor['valor']);
        }
        $nota['produtos'] = $produtos;
        $nota['data'] = mostraData($nota['creat_at']);
        $this->load('payment/payment_status', [
            'nota' => $nota,
        ]);

    }

    public function getLkink($return = false)
    {

        if (!Input::post('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::post('ID');
        $nota = $this->notas->getId($id);
        if (!$nota) {
            returnJson([
                'message' => "Nota não encontrada na base de dados",
                'status' => false
            ]);
        }

        if ($nota['id_status'] == 2) {
            returnJson([
                'message' => "O pagamento da nota já foi realizado",
                'status' => false
            ]);
        }
        $payload = json_encode([
            'ID' => $id,
            'KEY' => PAYMENT_SECRET_KEY
        ]);
        $token = sslEncrypt($payload);
        $link = '/payment?TOKEN=' . base64_encode($token);

        if ($return) {
            return $link;
        }
        returnJson([
            'link' => $link,
            'status' => true
        ]);
    }
    public function notificaction()
    {


        if (MERCADOPAGO_AMBIENTE == 'sandbox') {
            \MercadoPago\SDK::setAccessToken(MERCADOPAGO_ACCESS_TOKEN);
        } else {
            \MercadoPago\SDK::setAccessToken(MERCADOPAGO_ACCESS_TOKEN_PRODUCAO);
        }

        $dados = Input::post();
        if (empty($dados['data']->id)) {

            returnJson([
                'message' => 'Dados Invalidos',
                'status' => false
            ]);
        }
        $this->notification->insert($dados['data']->id, json_encode($dados));
        http_response_code(200);
        switch ($dados['type']) {
            case "payment":
                $payment = MercadoPago\Payment::find_by_id($dados['data']->id);
                break;
            case "plan":
                $plan = MercadoPago\Plan::find_by_id($dados['data']->id);
                break;
            case "subscription":
                $plan = MercadoPago\Subscription::find_by_id($dados['data']->id);
                break;
            case "invoice":
                $plan = MercadoPago\Invoice::find_by_id($dados['data']->id);
                break;
            case "point_integration_wh":
                $plan = $dados;
                break;
        }

        $dadosPayment =  isset($payment) ? $payment : $plan;





        if (empty($dadosPayment->external_reference)) {

            returnJson([
                'message' => 'Dados Invalidos',
                'status' => false
            ]);
        }

        $idNota = $dadosPayment->external_reference;
        $status = $dadosPayment->status;
        $dtPagamento = $dadosPayment->date_approved;
        $paymetType = $dadosPayment->payment_type_id;
        switch ($status) {
            case 'approved':

                $this->notas->updateColum($idNota, 'id_status', 2);
                $this->notas->updateColum($idNota, 'dt_pagamento', $dtPagamento);
                $this->notas->updateColum($idNota, 'payment_type', $paymetType);
                $this->notas->updateColum($idNota, 'mercado_pago_id', $dados['data']->id);


                break;
        }

        returnJson([
            'message' => 'Notification ok',
            'status' => true
        ]);
    }
}
