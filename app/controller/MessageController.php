<?php

namespace app\controller;

use app\core\Controller;

class  MessageController extends Controller
{

    public function __construct()
    {
    }

    public  function message(string $title, string $message,  $code = 404)
    {
        http_response_code($code);
        $this->load('message/message',[
            'title'=> $title,
            'message'=>$message
        ]
    );
    }

    public function notFound(){
        http_response_code(404);
        $this->load('error/notfound');
    }
}
