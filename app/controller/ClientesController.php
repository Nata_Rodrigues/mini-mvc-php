<?php

namespace app\controller;

use app\classes\Input;
use app\model\Clientes;
use app\core\Controller;

class  ClientesController extends Controller
{
    public $logado;
    public $clientes;

    public function __construct()
    {
        $this->logado = $this->allSessionPlataform();  // a this->logado ser� como uma session ativa
        $this->clientes = new Clientes;
    }

    public function index()
    {
        $this->load('clientes/clientes');
    }

    public function getAll()
    {

        returnJson($this->clientes->getAll());
    }

    public function getId()
    {

        if (!Input::get('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::get('ID');
        returnJson($this->clientes->getId($id));
    }

    public function insert()
    {

        if ($this->validateInsert()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $nome =  Input::post('NOME');
        $email =  Input::post('EMAIL');
        $contato =  Input::post('CONTATO');

        if ($this->clientes->insert($nome, $email, $contato)) {

            returnJson([
                'message' => "Cliente adicionado com sucesso!",
                'status' => true,
            ]);
        } else {
            returnJson([
                'message' => "Erro ao adicionar cliente, tente novamente.",
                'status' => false
            ]);
        }
    }

    public function delete()
    {
        if (!Input::delete('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::delete('ID');

        if ($this->clientes->delete($id)) {

            returnJson([
                'message' => "Cliente excluido com sucesso!",
                'status' => true,
            ]);
        } else {
            returnJson([
                'message' => "Erro ao excluir cliente, tente novamente.",
                'status' => false
            ]);
        }
    }

    public function salvar()
    {

        if (!$this->validateUpdate()) {
            $this->update();
        } elseif (!$this->validateInsert()) {
            $this->insert();
        } else {
            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }
    }

    public function update()
    {
        if ($this->validateUpdate()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::post('ID');
        if ($this->clientes->getId($id)) {
            $nome =  Input::post('NOME');
            $email =  Input::post('EMAIL');
            $contato =  Input::post('CONTATO');

            if ($this->clientes->update($id, $nome, $email, $contato)) {

                returnJson([
                    'message' => "Cliente atualizado com sucesso!",
                    'status' => true,
                ]);
            } else {
                returnJson([
                    'message' => "Erro ao atualizado cliente, tente novamente.",
                    'status' => false
                ]);
            }
        } else {
            returnJson([
                'message' => "Erro ao atualizado produto, tente novamente.",
                'status' => false
            ]);
        }
    }

    private function validateInsert()
    {
        if (!Input::post('NOME') or !Input::post('EMAIL') or (Input::post('CONTATO') == '')) {

            return true;
        } else {
            return false;
        }
    }

    private function validateUpdate()
    {

        if (!Input::post('NOME') or !Input::post('EMAIL') or (Input::post('CONTATO') == '') or (Input::post('ID') == '' or Input::post('ID') <= 0)) {

            return true;
        } else {
            return false;
        }
    }
}
