<?php

namespace app\controller;


use app\classes\Input;
use app\classes\PDF;
use app\model\Notas;
use app\model\ItemNotas;
use app\model\Clientes;
use app\model\Produtos;
use app\model\Status;
use app\model\Usuarios;
use app\core\Controller;
use app\controller\PaymentController;
use app\controller\EmailController;



class  NotasController extends Controller
{

    public $logado;
    public $notas;
    public $itemNotas;
    public $clientes;
    public $produtos;
    public $status;
    public $pdf;
    public $email;
    public $usuario;
    public $payment;


    public function __construct()
    {
        $this->logado = $this->allSessionPlataform();
        $this->notas = new Notas;
        $this->itemNotas = new ItemNotas;
        $this->clientes = new Clientes;
        $this->produtos = new Produtos;
        $this->status = new Status;
        $this->usuario = new Usuarios;
        $this->payment = new PaymentController;       
        $this->pdf = new PDF;

    }

    public function index()
    {
        $clientes = $this->clientes->getAll();
        $produtos = $this->produtos->getAll();
        $status = $this->status->getAll();
        $this->load('notas/notas', [
            'clientes' => $clientes,
            'produtos' => $produtos,
            'status' => $status
        ]);
    }
    public function pesquisa()
    {
        $dados =  Input::post();

        $notas = $this->notas->pesquisa($dados);
        if ($notas) {
            foreach ($notas as $index => $value) {
                $totalNota = $this->itemNotas->getValorTotal($value['id']);
                $notas[$index]['valorTotal'] = $totalNota['valorTotal'];
                $cliente = $this->clientes->getId($value['id_cliente']);
                $notas[$index]['cliente'] = $cliente['nome'];
                $status = $this->status->getId($value['id_status']);
                $notas[$index]['status'] = $status['descricao'];
                $notas[$index]['cor_status'] = $status['cor'];
            }
        }
        returnJson($notas);
    }
    public function getAll()
    {
        $notas = $this->notas->getAll();
        if ($notas) {
            foreach ($notas as $index => $value) {
                $totalNota = $this->itemNotas->getValorTotal($value['id']);
                $notas[$index]['valorTotal'] = $totalNota['valorTotal'];
                $cliente = $this->clientes->getId($value['id_cliente']);
                $notas[$index]['cliente'] = $cliente['nome'];
                $status = $this->status->getId($value['id_status']);
                $notas[$index]['status'] = $status['descricao'];
                $notas[$index]['cor_status'] = $status['cor'];
                $produtos = $this->itemNotas->findBy('id_nota', $value['id']);
                foreach ($produtos as $key => $produto) {
                    $dadosProduto = $this->produtos->getId($produto['id_produto']);
                    $produtos[$key]['descricao'] = $dadosProduto['descricao'];
                }

                $notas[$index]['produtos'] = $produtos;
            }
        }
        returnJson($notas);
    }

    public function getId($return = false)
    {

        if (!Input::get('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::get('ID');
        if ($nota = $this->notas->getId($id)) {
            $totalNota = $this->itemNotas->getValorTotal($id);
            $nota['valorTotal'] = $totalNota['valorTotal'];
            $cliente = $this->clientes->getId($nota['id_cliente']);
            $nota['cliente'] = $cliente['nome'];
            $nota['id_cliente'] = $cliente['id'];
            $status = $this->status->getId($nota['id_status']);
            $nota['id_status'] = $status['id'];
            $nota['status'] = $status['descricao'];
            $nota['cor_status'] = $status['cor'];
            $produtos = $this->itemNotas->findBy('id_nota', $id);
            foreach ($produtos as $index => $produto) {
                $dadosProduto = $this->produtos->getId($produto['id_produto']);
                $produtos[$index]['descricao'] = $dadosProduto['descricao'];
            }
            $nota['produtos'] = $produtos;


            if ($return) {
                return $nota;
            }
            returnJson($nota);
        } else {
            returnJson([
                'message' => "Nota não encontrada na base de dados",
                'status' => false
            ]);
        }
    }

    public function insert()
    {

        if ($this->validateInsert()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $idCliente =  Input::post('ID_CLIENTE');
        $codStatus = Input::post('STATUS');
        $produtos = Input::post('PRODUTOS');
        $pagamento = !empty(Input::post('PAGAMENO')) ? Input::post('PAGAMENO') : Null;
        $idUsuario = $_SESSION['ID_USER'];
        $idNota = $this->notas->insert($idCliente, $codStatus, $pagamento, $idUsuario);
        if ($idNota) {
            foreach ($produtos as $value) {
                $idProduto = $value->id;
                $valor = $value->valor;
                $quantidade = $value->quantidade;
                $this->itemNotas->insert($idNota, $idProduto, $valor, $quantidade);
            }

            returnJson([
                'message' => "Nota adicionada com sucesso!",
                'status' => true,
            ]);
        } else {
            returnJson([
                'message' => "Erro ao adicionar produto, tente novamente.",
                'status' => false
            ]);
        }
    }

    public function delete()
    {
        if (!Input::delete('ID')) {
            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::delete('ID');

        if ($nota = $this->notas->getId($id)) {

            if ($nota['id_status'] == 2) {
                returnJson([
                    'message' => "Notas com status 'Pago' não podem ser excluidas.",
                    'status' => false
                ]);
            }
            if ($this->itemNotas->deleteNota($id)) {
                if ($this->notas->delete($id)) {

                    returnJson([
                        'message' => "Nota excluida com sucesso!",
                        'status' => true,
                    ]);
                } else {
                    returnJson([
                        'message' => "Erro  ao excluir nota, tente novamente.",
                        'status' => false
                    ]);
                }
            } else {
                returnJson([
                    'message' => "Erro  ao excluir nota, tente novamente.",
                    'status' => false
                ]);
            }
        } else {
            returnJson([
                'message' => "Nota não cadastrada na base de dados.",
                'status' => false
            ]);
        }
    }

    public function salvar()
    {

        if (!$this->validateUpdate()) {
            $this->update();
        } elseif (!$this->validateInsert()) {
            $this->insert();
        } else {
            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }
    }

    public function update()
    {
        if ($this->validateUpdate()) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }


        $id = Input::post('ID');
        if ($nota = $this->notas->getId($id)) {

            if ($nota['id_status'] == 2) {
                returnJson([
                    'message' => "Notas com status 'Pago' não podem ser alteradas.",
                    'status' => false
                ]);
            }
            $idCliente =  Input::post('ID_CLIENTE');
            $codStatus = Input::post('STATUS');
            $produtos = Input::post('PRODUTOS');
            $pagamento = !empty(Input::post('PAGAMENO')) ? Input::post('PAGAMENO') : Null;
            $idUsuario = $_SESSION['ID_USER'];

            if ($this->notas->update($id, $idCliente, $codStatus, $pagamento, $idUsuario)) {

                if ($this->itemNotas->deleteNota($id)) {
                    foreach ($produtos as $value) {
                        $idProduto = $value->id;
                        $valor = $value->valor;
                        $quantidade = $value->quantidade;
                        $this->itemNotas->insert($id, $idProduto, $valor, $quantidade);
                    }
                    returnJson([
                        'message' => "Nota atualizada com sucesso!",
                        'status' => true,
                    ]);
                } else {
                    returnJson([
                        'message' => "Erro ao atualizadar nota, tente novamente.",
                        'status' => false
                    ]);
                }
            } else {
                returnJson([
                    'message' => "Erro ao atualizadar nota, tente novamente.",
                    'status' => false
                ]);
            }
        } else {
            returnJson([
                'message' => "Erro ao atualizado nota, nota inexistente na base de dados.",
                'status' => false
            ]);
        }
    }

    private function validateInsert()
    {
        if (empty(Input::post('PRODUTOS')) or (Input::post('STATUS') == '' or  Input::post('STATUS') < 0) or (Input::post('ID_CLIENTE') == '' or Input::post('ID_CLIENTE') < 0)) {

            return true;
        } else {
            return false;
        }
    }

    private function validateUpdate()
    {
        if (empty(Input::post('PRODUTOS')) or (Input::post('STATUS') == '' or  Input::post('STATUS') < 0) or (Input::post('ID_CLIENTE') == '' or Input::post('ID_CLIENTE') < 0) or (Input::post('ID') == '' or Input::post('ID') <= 0)) {

            return true;
        } else {
            return false;
        }
    }

    public function comprovante()
    {

        if (!Input::get('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }
        $id = Input::get('ID');
        if ($this->notas->getId($id)) {

            $nota = $this->getId(true);
            $this->pdf->comprovante($nota);
        } else {
            returnJson([
                'message' => "Nota não encontrada na base de dados",
                'status' => false
            ]);
        }
    }

    public function enviaCobranca()
    {


        if (!Input::post('ID')) {

            returnJson([
                'message' => "Dados Invalidos",
                'status' => false
            ]);
        }

        $id = Input::post('ID');
        $nota = $this->notas->getId($id);
        if (!$nota) {
            returnJson([
                'message' => "Nota não encontrada na base de dados",
                'status' => false
            ]);
        }

        if ($nota['id_status'] == 2) {
            returnJson([
                'message' => "O pagamento da nota já foi realizado",
                'status' => false
            ]);
        }

        $totalNota = $this->itemNotas->getValorTotal($id);
        $nota['valorTotal'] = valorFormatado($totalNota['valorTotal']);
        $cliente = $this->clientes->getId($nota['id_cliente']);
        $nota['cliente'] = $cliente['nome'];
        $nota['email_cliente'] = $cliente['email'];
        $produtos = $this->itemNotas->findBy('id_nota', $id);
        foreach ($produtos as $index => $produto) {
            $dadosProduto = $this->produtos->getId($produto['id_produto']);
            $produtos[$index]['descricao'] = $dadosProduto['descricao'];
            $produtos[$index]['valor'] = valorFormatado($dadosProduto['valor']);
        }
        $nota['produtos'] = $produtos;
        $nota['data'] = mostraData($nota['creat_at']);

        $link = $this->payment->getLkink(true);
        EmailController::cobranca($link, $nota);
    }
}
