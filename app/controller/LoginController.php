<?php

namespace app\controller;

use app\classes\Input;
use app\classes\Jwt;
use app\core\Controller;
use app\model\Authorization;
class LoginController extends Controller
{


    public function __construct()
    {
        session_start();

    }
    public function index()
    {
        $this->load('login/login');
    }
    public function Login()
    {
   
        try {

            $email =  Input::post('email');
            $password =  Input::post('password');
            $jwt = new Jwt;

            if (empty($email) or empty($password)) {

                $retorno = json_encode([
                    'message' => "Dados nao enviados",
                    'status' => false
                ]);

                echo $retorno;
                exit;
            }

            $password = Encript($password);
            $login = new Authorization();
            $usuario = $login->Login($email,$password);
            if ($usuario) {

                $dados = array(
                    "ID"        => $usuario['id'],
                    "NOME"      => $usuario['nome'],
                    "TOKEN"     => $usuario['token'],
                    "AUTH"      => true,
                    "TIME"      => time()
                );
                $array = $jwt->gerarJwt($dados);
                $response = json_encode([
                    "status" => true,
                    "jwt" => $array['jwt'],
                    "data_expira" => $array['data_expira'],
                ]);
                $_SESSION['TOKEN'] = 'Bearer '.$array['jwt'];
                $_SESSION['ID_USER'] = $usuario['id'];
                echo $response;
                exit;
            } else {

                $response = json_encode([
                    "status" => false,
                    'message' => "Login ou Senha incorretos",

                ]);

                echo $response;
                exit;
            }
        } catch (\Exception $th) {
            $response = array(
                "status" => "Erro",
                'erro' => $th
            );

            echo json_encode($response);
            exit;
        }
    }

    public function logout(){
        session_destroy();
        returnJson([
            "status" => true,
            'message' => 'Deslogado'
        ]); 
    }
}
