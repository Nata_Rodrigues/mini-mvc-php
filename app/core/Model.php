<?php
namespace app\core;
use PDOException;

class Model {

	protected $db;
	//protected $tablename;

	public function __construct() {
		global $db;
		$this->db = $db;
		//$this->tablename = $tablename;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value){
		try{
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}
}