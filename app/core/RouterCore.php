<?php

namespace app\core;


class RouterCore
{
    private $uri;
    private $method;
    private $getArr = [];
    private $postArr = [];

    public $message;
    public function __construct()
    {
        $this->initialize();
        require_once('../app/config/router.php');
        $this->execute();
    }

    private function initialize()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];
        if (strpos($uri, '?')) {
            $uri = mb_substr($uri, 0, strpos($uri, '?'));
        }
        $uri = explode('/', $uri);
        $uri = $this->normalizeURI($uri);
        for ($i = 0; $i < UNSET_URI_COUNT; $i++) {
            unset($uri[$i]);
        }

        $this->uri = implode('/', $this->normalizeURI($uri));
        if (DEBUG_URI) {
            dd($this->uri);
        }
    }

    private function get($router, $call)
    {
        $this->getArr[] = [
            'router' => $router,
            'call' => $call
        ];
    }

    private function post($router, $call)
    {
        $this->postArr[] = [
            'router' => $router,
            'call' => $call
        ];
    }
    private function delete($router, $call)
    {
        $this->postArr[] = [
            'router' => $router,
            'call' => $call
        ];
    }
    private function execute()
    {

        switch ($this->method) {

            case 'GET':
                $this->executeGET();

                break;
            case 'POST':
                $this->executePOST();
                break;
            case 'DELETE':
                $this->executeDELETE();
                break;
        }
    }
    private function executeGET()
    {
        $validateUri = false;
        foreach ($this->getArr as $get) {
            $r = substr($get['router'], 1);
            if (substr($r, -1) == '/') {
                $r = substr($r, 0, -1);
            }

            if ($this->uri === $r) {



                if (is_callable($get['call'])) {
                    $get['call']();
                    break;
                } else {
                    $this->executeController($get['call']);
                    $validateUri = true;

                    exit;
                }
            }
        }
        if (!$validateUri) {
            (new \app\controller\MessageController)->notFound();
            return;
        }
    }
    private function executePOST()
    {
        $validateUri = false;
        foreach ($this->postArr as $post) {
            $r = substr($post['router'], 1);
            if (substr($r, -1) == '/') {
                $r = substr($r, 0, -1);
            }
            if ($this->uri === $r) {
                if (is_callable($post['call'])) {
                    $post['call']();
                    break;
                } else {
                    $this->executeController($post['call']);
                    $validateUri = true;
                }
            }
        }

        if (!$validateUri) {
            (new \app\controller\MessageController)->notFound();
            return;
        }
    }
    private function executeDELETE()
    {
        $validateUri = false;
        foreach ($this->postArr as $post) {
            $r = substr($post['router'], 1);
            if (substr($r, -1) == '/') {
                $r = substr($r, 0, -1);
            }
            if ($this->uri === $r) {
                if (is_callable($post['call'])) {
                    $post['call']();
                    break;
                } else {
                    $this->executeController($post['call']);
                    $validateUri = true;
                }
            }
        }

        if (!$validateUri) {
            (new \app\controller\MessageController)->notFound();
            return;
        }
    }
    private function executeController($get)
    {
        try{
        $ex = explode('@', $get);
        if (!isset($ex[0]) || !isset($ex[1])) {
            (new \app\controller\MessageController)->message('Dados Invalidos', 'Controller ou Methodo nao encontrado "' . $get . '"', 404);
            return;
        }

        $cont = 'app\\controller\\' . $ex[0];
        if (!class_exists($cont)) {
            
            (new \app\controller\MessageController)->message('Dados Invalidos', 'Controller nao encontrado "' . $get . '"', 404);
            return;
        }

        if (!method_exists($cont, $ex[1])) {
            (new \app\controller\MessageController)->message('Dados Invalidos', 'Methodo nao encontrado "' . $get . '"', 404);
            return;
        }
 
        call_user_func_array([
            new $cont,
            $ex[1]
        ], []);
    }catch(\Exception $e){
        echo $e->getMessage();
    }
    }
    private function normalizeURI($arr)
    {
        $arr = array_values(array_filter($arr));
        return $arr;
    }
}
