<?php

namespace app\core;

use app\classes\Jwt;
use app\classes\Input;
class Controller
{



    protected function load(string $view, $params = [])
    {

        $loader = new \Twig\Loader\FilesystemLoader('../app/view/');
        $twig = new \Twig\Environment($loader);

        $twig->addGlobal('BASE', BASE);
        $twig->addGlobal('APP_NAME', APP_NAME);

        echo $twig->render($view . '.php', $params);
    }

    public function getJwt()
    {
        $header = apache_request_headers();
        if (empty($header['Authorization']) && empty($header['authorization'])) {
            session_start();
            if (isset($_SESSION['TOKEN'])) {
                $header = $_SESSION;
            } else {
                $this->load('login/login');
                exit;
            }
        }
        return $header;
    }

    public function allSessionPlataform()
    {

        $jwt = new Jwt();
        $header = $this->getJwt();
        $logado = $jwt->validarJwt($header);
        $tipo_acesso = isset($logado['TIPO_ACESSO']) ? $logado['TIPO_ACESSO'] : null;
        $agente  = isset($logado['AGENTE']) ? $logado['AGENTE'] : null;


        if ($logado['STATUS'] == "LOGADO") {
            $array = array(
                "ID" => $logado['ID'],
                "data_expira"  => $logado['data_expira'],
                "TIPO_ACESSO" => $tipo_acesso,
                "AGENTE" => $agente
            );

            return $array;
        } else if (empty($header['Authorization']) && empty($header['authorization'])) {
            $this->load('login/login');
            exit;
        } else {
            $array['CODIGO'] = $logado['CODIGO'];
            $array['MENSAGEM'] = $logado['MENSAGEM'];
            $array['HTTP_CODE'] = $logado['HTTP_CODE'];
            returnJson($array);
        }
    }
}
