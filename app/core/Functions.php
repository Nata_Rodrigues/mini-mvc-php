<?php

function dd($params, $die = true)
{

    if (is_array($params)) {
        echo '<pre>';
        print_r($params);
        echo '</pre>';
    } else {
        echo '<pre>';
        echo ($params);
        echo '</pre>';
    }
    if ($die) {
        die;
    }
}


function returnJson($payload, $exit = true)
{
    if (isset($payload)) {
        $return = json_encode($payload);
        echo $return;
        if ($exit) {
            exit;
        }
    } else {
        echo "Payload Vazio";
        exit;
    }
}

function Encript($password)
{

    $password = md5($password);
    return hash_hmac("sha256", $password, md5(JWT_SECRET_KEY));
}

function gravaValor($data)
{


    $data = str_replace(".", "", $data);
    $data = str_replace(",", ".", $data);

    return $data;
}


function formatCep($value)
{
    return substr($value, 0, 5) . '-' . substr($value, 5, 3);
}

function mostraData($data)
{
    if ($data != '') {
        return (substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4));
    }
}



function valorPorExtenso($valor = 0)
{
    $singular = array("Centavo", "Real", "Mil", "Milhão", "Bilhão", "Trilhão", "Quatrilhão");
    $plural = array(
        "Centavos", "Reais", "Mil", "Milhões", "Bilhões", "Trilhões",
        "Quatrilhões"
    );

    $c = array(
        "", "Cem", "Duzentos", "Trezentos", "Quatrocentos",
        "Quinhentos", "Seiscentos", "Setecentos", "Oitocentos", "Novecentos"
    );
    $d = array(
        "", "Dez", "Vinte", "Trinta", "Quarenta", "Cinquenta",
        "sessenta", "setenta", "oitenta", "noventa"
    );
    $d10 = array(
        "Dez", "Onze", "Doze", "Treze", "Quatorze", "Quinze",
        "Dezesseis", "Dezesete", "Dezoito", "Dezenove"
    );
    $u = array(
        "", "Um", "Dois", "Três", "Quatro", "Cinco", "Seis",
        "Sete", "Oito", "Nove"
    );

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    for ($i = 0; $i < count($inteiro); $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    $rt = "";
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
            $ru) ? " e " : "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000")
            $z++;
        elseif ($z > 0) $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];


        $rt = $rt . ((($i > 0) && ($i <= $fim) &&
            ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    return trim($rt ? $rt : "zero");
}

function valorFormatado($valor = 0)
{
    return number_format($valor, 2, ',', '.');
}

function sslEncrypt($text)
{
    $plaintext = $text;
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, JWT_SECRET_KEY, $options = OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, JWT_SECRET_KEY, $as_binary = true);
    $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);
    return $ciphertext;
}

function sslDecrypt($text)
{
    //decrypt later....
    $c = base64_decode($text);
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, JWT_SECRET_KEY, $options = OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, JWT_SECRET_KEY, $as_binary = true);
    if (hash_equals($hmac, $calcmac)) // timing attack safe comparison
    {
        return $original_plaintext;
    }
}
