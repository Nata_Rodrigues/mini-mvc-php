<?php

namespace app\classes;

use app\model\Message;
use Core\Controller;

define("_VALIDADE_TOKEN_MINUTOS_", 15);
define("_JWT_SECRET_", JWT_SECRET_KEY);




class Jwt
{



	private $numCriptografa = array(
		"etapa1" => 2,
		"etapa3" => 1,
		"etapa5" => 3
	);

	private function getNumCriptografa($etapa)
	{
		return $this->numCriptografa[$etapa];
	}

	/* PARTE DE GERAR O JWT */
	public function gerarJwt($data)
	{
		if (isset($_GET['DEBUG'])) {
			echo "gerar";

			print_r($data);
		}

		$array = array();
		if (empty($data['ID']) or  empty($data['TOKEN'])) {
			returnJson(array('externalApiResponse' => array('message' =>  "Dados não enviados para o JWT")), 403);
		}

		$array['jwt'] = $this->create($data, $data_expira);
		$array['data_expira'] = $data_expira;


		return $array;
	}

	public function create($data, &$data_expira)
	{
		if (isset($_GET['DEBUG'])) {
			echo "create";
		}
		global $config;

		$header = json_encode(array("typ" => "JWT", "alg" => "HS256"));
		$data['data_expira'] = date('Y-m-d H:i:s', strtotime('+' . _VALIDADE_TOKEN_MINUTOS_ . ' minutes'));
		$data_expira = $data['data_expira'];

		$payload = json_encode($data);

		$hbase = $this->base64url_encode($header);

		$pbase = $this->gerarCriptografia($payload);


		$signature = hash_hmac("sha256", $hbase . "." . $pbase, md5(_JWT_SECRET_), true);

		$bsig = $this->base64url_encode($signature);

		$jwt = $hbase . "." . $pbase . "." . $bsig;

		return $jwt;
	}

	private function percorrerCriptografia($valor, $funcao, $quantidade)
	{

		for ($i = 0; $i < $quantidade; $i++) {
			$valor = $this->$funcao($valor);
		}

		return $valor;
	}

	private function gerarCriptografia($hash)
	{
		/*
		Primeira etapa
		Gerar base64url_encode de acordo com o número atribuído no array $numCriptografa
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_encode', $this->getNumCriptografa("etapa1"));

		/*
		Segunda etapa
		Trocar os caracteres "V" -> "xfs"; "T" -> "57"
		*/
		$hash = str_replace("V", "xfs", $hash);
		$hash = str_replace("T", "57", $hash);

		/*
		Terceira etapa
		Gerar base64url_encode de acordo com o número atribuído no array $numCriptografa 
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_encode', $this->getNumCriptografa("etapa3"));

		/*
		Quarta etapa
		Trocar os caracteres "h" -> "grj"
		*/
		$hash = str_replace("h", "gjr", $hash);

		/*
		Quinta etapa (última)
		Gerar base64url_encode de acordo com o número atribuído no array $numCriptografa 
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_encode', $this->getNumCriptografa("etapa5"));

		return $hash;
	}

	private function base64url_encode($data)
	{
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	/* =====================================================================================================*/

	/* PARTE DE VALIDAR O JWT */
	public function validarJwt($header)
	{

		$array = array();
		$authorization  = '';

		if (isset($_GET['DEBUG'])) {
			echo "HEADER: ";
			print_r($header);
			echo ">>>>>>>>>>>>>>>";
			echo "authorization: $header[authorization] \n";
		}
		$array_authorization = array();
		if (isset($header['authorization'])) {
			$array_authorization = explode(" ", $header['authorization']);
			$authorization = $header['authorization'];
		} elseif (isset($header['Authorization'])) {
			$array_authorization = explode(" ", $header['Authorization']);
			$authorization = $header['Authorization'];
		} else {
			if (isset($header['TOKEN'])) {
				$array_authorization = explode(" ", $header['TOKEN']);
				$authorization = $header['TOKEN'];
			}
		}
		//print_r($array_authorization) ;
		if (isset($_GET['DEBUG'])) {
			echo $array_authorization[0];
			echo '<<<';

			echo count($array_authorization);
			echo "<<<";

			print_r($array_authorization[1]);
			echo "<<<<";
		}

		if (count($array_authorization) != 2 or $array_authorization[0] != 'Bearer' or $array_authorization[1] == 'null') {

			returnJson(array('externalApiResponse' => array('message' => "Authorization não enviado")), 401);
		}

		return $this->validateJwt($authorization);
	}

	public function validateJwt($token)
	{
		if (isset($_GET['DEBUG'])) {
			echo "TOKEN: $token \n";
		}

		$token = explode(' ', $token);
		$token = $token[1];

		if (isset($_GET['DEBUG'])) {
			echo "TOKEN: $token \n";
		}
		$valida = json_decode($this->validate($token));

		if (isset($_GET['DEBUG'])) {
			print_r($valida);
		}

		if (empty($valida->{'ID'})) {
			$validar['STATUS'] = 'ACESSO NEGADO';
			$validar['CODIGO'] = 'C10.006';
			$mensagem = new Message;
			$retorno = $mensagem->getId($validar['CODIGO']);
			$validar['MENSAGEM'] = $retorno['MENSAGEM'];
			$validar['HTTP_CODE'] = $retorno['HTTP_CODE'];

		} else {
			$info = (array) json_decode($this->validate($token));

			$validar = array();

			if (!empty($info["ID"])  && !empty($info["data_expira"])) {

				if (date('Y-m-d H:i:S') <= $info["data_expira"]) {
					$validar = array_merge($validar, $info);
					$validar['STATUS'] = 'LOGADO';
					$validar['CODIGO'] = 'C10.004';

					$mensagem = new Message;
					$retorno = $mensagem->getId($validar['CODIGO']);
					$validar['MENSAGEM'] = $retorno['MENSAGEM'];
					$validar['HTTP_CODE'] = $retorno['HTTP_CODE'];

				} else {
					$validar['STATUS'] = 'VENCIDO';
					$validar['CODIGO'] = 'C10.005';

					$mensagem = new Message;
					$retorno = $mensagem->getId($validar['CODIGO']);
					$validar['MENSAGEM'] = $retorno['MENSAGEM'];
					$validar['HTTP_CODE'] = $retorno['HTTP_CODE'];

				}
			} else {
				$validar['STATUS'] = 'ACESSO NEGADO';
				$validar['CODIGO'] = 'C10.006';

				$mensagem = new Message;
				$retorno = $mensagem->getId($validar['CODIGO']);
				$validar['MENSAGEM'] = $retorno['MENSAGEM'];
				$validar['HTTP_CODE'] = $retorno['HTTP_CODE'];

			}
		}



		return $validar;
	}

	private function validate($jwt)
	{
		global $config;
		$array = array();

		$jwt_splits = explode('.', $jwt);

		if (count($jwt_splits) == 3) {
			$signature = hash_hmac("sha256", $jwt_splits[0] . "." . $jwt_splits[1], md5(_JWT_SECRET_), true);

			$bsig = $this->base64url_encode($signature);

			if ($bsig == $jwt_splits[2]) {

				$array = $this->descriptografar($jwt_splits[1]);
			} else {
				echo "ERRO: JWT inválido!";
				exit;
			}
		}

		return $array;
	}

	private function descriptografar($hash)
	{
		/*
		Primeira etapa
		Gerar base64url_decode de acordo com o número atribuído no array $numCriptografa
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_decode', $this->getNumCriptografa("etapa5"));

		/*
		Segunda etapa
		Trocar os caracteres "gjr" -> "h"
		*/
		$hash = str_replace("gjr", "h", $hash);

		/*
		Terceira etapa
		Gerar base64url_decode de acordo com o número atribuído no array $numCriptografa 
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_decode', $this->getNumCriptografa("etapa3"));

		/*
		Quarta etapa
		Trocar os caracteres "57" -> "T"; "xfs" -> "V"
		*/
		$hash = str_replace("57", "T", $hash);
		$hash = str_replace("xfs", "V", $hash);

		/*
		Quinta etapa
		Gerar base64url_decode de acordo com o número atribuído no array $numCriptografa
		*/
		$hash = $this->percorrerCriptografia($hash, 'base64url_decode', $this->getNumCriptografa("etapa1"));

		return $hash;
	}

	private function base64url_decode($data)
	{
		return base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen($data)) % 4));
	}
}
