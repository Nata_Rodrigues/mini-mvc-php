<?php


namespace app\classes;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


class Email {


    private $mail;

   public function __construct(){
        $this->mail = new PHPMailer(true);
        $this->mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
        $this->mail->isSMTP();                                            //Send using SMTP
        $this->mail->Host       = MAIL_HOST;                     //Set the SMTP server to send through
        $this->mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $this->mail->Username   = MAIL_USERNAME;                     //SMTP username
        $this->mail->Password   = MAIL_PASSWORD;                               //SMTP password
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
        $this->mail->Port       = 587;  
    }


    public  function enviar ($remetente,$destinatario,$titleEmail,$bodyEmail,$anexo = null){

        try {
        $this->mail->setFrom($remetente);
        $this->mail->addAddress($destinatario);               //Name is optional
   
        //Content
        $this->mail->isHTML(true);                                  //Set email format to HTML
        $this->mail->Subject = $titleEmail;
        $this->mail->Body    = $bodyEmail;
    
        $this->mail->send();
        returnJson([
            'message' => "Email enviado com sucesso",
            'status' => true
        ]);
    } catch (Exception $e) {
        returnJson([
            'message' => $this->mail->ErrorInfo,
            'status' => false
        ]);
    }
    }

}
