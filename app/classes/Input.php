<?php

namespace app\classes;

class Input
{
    public static function get(string $param = null)
    {

        $data = file_get_contents('php://input');
        if (empty($data)) {
            $data = $_GET;
        }
        if (!empty($data)) {
            if ($param) {
                if (isset($data[$param])) {
                    $data_r = $data[$param];
                } else {
                    $data_r = false;
                }
            } else {
                $data_r = (array) $data;
            }

            return  $data_r;
        } else {
            $response = json_encode([
                'status' => false,
                'message' => 'Dados nao enviados'
            ]);
        }
    }

    public static function post(string $param = null)
    {

        $data = json_decode(file_get_contents('php://input'));
        if (empty($data)) {
            $data = $_POST;
        }
        if (!empty($data)) {
            if ($param) {
                if (isset($data->$param)) {
                    $data_r = $data->$param;
                } else {
                    $data_r = false;
                }
            } else {
                $data_r = (array) $data;
            }

            return  $data_r;
        } else {
            $response = json_encode([
                'status' => false,
                'message' => 'Dados nao enviados'
            ]);
        }
    }

    public static function delete(string $param = null)
    {

        $data = json_decode(file_get_contents('php://input'));

        if (!empty($data)) {
            if ($param) {
                if (isset($data->$param)) {
                    $data_r = $data->$param;
                } else {
                    $data_r = false;
                }
            } else {
                $data_r = (array) $data;
            }

            return  $data_r;
        } else {
            $response = json_encode([
                'status' => false,
                'message' => 'Dados nao enviados'
            ]);
        }
    }
}
