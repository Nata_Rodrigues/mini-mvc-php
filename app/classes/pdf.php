<?php

namespace app\classes;

use app\classes\TCPDF\tcpdf as MYPDF;

 class PDF
{

    public $pdf;

    public function __construct()
    {
        $this->pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->pdf->SetHeaderMargin(0);
        $this->pdf->SetFooterMargin(0);
        $this->pdf->setPrintFooter(false);
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    }


    public function comprovante($dados)
    {

        $produtos = $dados['produtos'];
        $this->pdf->SetMargins(60, 10, 60);
        $this->pdf->setPrintHeader(false);
        $w = 90;
        $h = 5;
        $this->pdf->SetFont('helvetica', '', 9);

        $this->pdf->AddPage();
        $this->pdf->Cell($w, $h, '', 'LTR', 1, 'C');
        $this->pdf->Cell($w, $h, APP_NAME, 'LR', 1, 'C');
        $this->pdf->Cell($w, $h, '', 'LBR', 1, 'C');
        $this->pdf->Cell($w, $h, 'Comprovante', 'LR', 1, 'C');
        $this->pdf->Cell($w, $h, '', 'LR', 1, 'C');
        $this->pdf->Cell($w / 2, $h, ' Nota Nº: ' . $dados['id'], 'L', 0, 'L');
        $this->pdf->Cell($w / 2, $h, ' Data: ' . mostraData($dados['creat_at']).' ', 'R', 1, 'R');
        $this->pdf->Cell($w, $h, ' Cliente: ' . $dados['cliente'], 'LR', 1, 'L');
        $this->pdf->Cell($w, $h, '', 'LR', 1, 'C');
        $this->pdf->Cell($w / 4, $h, ' #Cod ', 'L', 0, 'L');
        $this->pdf->Cell($w / 4, $h, 'Produto ', 0, 0, 'L');
        $this->pdf->Cell($w / 4, $h, 'Qnt ', 0, 0, 'C');
        $this->pdf->Cell($w / 4, $h, 'Valor ', 'R', 0, 'R');

        $this->pdf->Cell($w, $h, '', 'LR', 1, 'C');
        foreach ($produtos as $produto) {
            $this->pdf->Cell($w / 4, $h,' '. $produto['id'], 'L', 0, 'L');
            $this->pdf->Cell($w / 4, $h, $produto['descricao'], 0, 0, 'L');
            $this->pdf->Cell($w / 4, $h, $produto['quantidade'], 0, 0, 'C');
            $this->pdf->Cell($w / 4, $h, 'R$: ' . valorFormatado($produto['valor']).' ', 'R', 1, 'R');
        }
        $this->pdf->Cell($w, $h, '', 'LR', 1, 'C');
        $this->pdf->Cell($w / 4, $h, ' Total: ', 'L', 0, 'l');
        $this->pdf->Cell($w / 4, $h, '', 0, 0, 'C');
        $this->pdf->Cell($w / 4, $h, '', 0, 0, 'C');
        $this->pdf->Cell($w / 4, $h, ' R$: ' . valorFormatado($dados['valorTotal']).' ', 'R', 1, 'R');
        $this->pdf->Cell($w, $h, '', 'LBR', 1, 'C');
        $this->pdf->Cell($w, $h, '', 'LR', 1, 'C');
        $this->pdf->Cell($w / 2, $h, ' Payment_id: '.  $dados['mercado_pago_id'] , 'L', 0, 'L');
        $this->pdf->Cell($w / 2, $h, ' Pagamento: '. mostradata($dados['dt_pagamento']), 'R', 1, 'R');
        $this->pdf->Cell($w, $h, '', 'LBR', 1, 'C');










        $this->pdf->Output('nota'.$dados['id'].'.pdf', 'I');
    }
}
