<?php


global $config;

$config = array();
$config['host'] = DB_HOST;
$config['dbname'] = DB_NAME;
$config['dbuser'] = DB_USER;
$config['dbpass'] = DB_PASS;
$config['jwt_secret_key'] = JWT_SECRET_KEY;

global $db;
try {
    $db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'], $config['dbuser'], $config['dbpass']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES utf8');
} catch(PDOException $e) {
    $array = array();
    header("HTTP/1.0 403");
    $array['mensagem'] = "Falha na requisição.";
    echo json_encode($array);
    exit;
}
