<?php
//GET
/*<< Telas >>*/
$this->get('/', 'LoginController@index');
$this->get('/home', 'HomeController@index');
$this->get('/produtos', 'ProdutosController@index');
$this->get('/clientes', 'ClientesController@index');
$this->get('/notas', 'NotasController@index');
$this->get('/payment', 'PaymentController@index');

/*<< Produtos >>*/
$this->get('/produtos/getAll', 'ProdutosController@getAll');
$this->get('/produtos/getId', 'ProdutosController@getId');

/*<< Clientes >>*/
$this->get('/clientes/getAll', 'ClientesController@getAll');
$this->get('/clientes/getId', 'ClientesController@getId');

/*<< Notas >>*/
$this->get('/notas/getAll', 'NotasController@getAll');
$this->get('/notas/getId', 'NotasController@getId');
$this->get('/notas/comprovante', 'NotasController@comprovante');


/*<< PAYMENT >>*/
$this->get('/payment/success', 'PaymentController@paymentSuccess');
$this->get('/payment/failure', 'PaymentController@paymentFailure');
$this->get('/payment/pending', 'PaymentController@paymentPending');



/*<------------------------------------------------------------------------------------------------------------>*/

//POST
/*<< Login >>*/
$this->post('/login', 'LoginController@login');
$this->post('/logout', 'LoginController@logout');

/*<< Usuario >>*/
$this->post('/usuario/inserir', 'UsuariosController@insert');

/*<< Produtos >>*/
$this->post('/produtos/salvar', 'ProdutosController@salvar');
$this->post('/produtos/update', 'ProdutosController@update');
$this->post('/produtos/inserir', 'ProdutosController@insert');

/*<< Clientes >>*/
$this->post('/clientes/salvar', 'ClientesController@salvar');
$this->post('/clientes/update', 'ClientesController@update');
$this->post('/clientes/inserir', 'ClientesController@insert');

/*<< Notas >>*/
$this->post('/notas/salvar', 'NotasController@salvar');
$this->post('/notas/update', 'NotasController@update');
$this->post('/notas/inserir', 'NotasController@insert');
$this->post('/notas/pesquisa', 'NotasController@pesquisa');
$this->post('/notas/enviaCobranca', 'NotasController@enviaCobranca');

/*<< PAYMENT >>*/
$this->post('/payment/save', 'PaymentController@paymentSave');
$this->post('/payment/getLink', 'PaymentController@getLkink');
$this->post('/payment/notificaction', 'PaymentController@notificaction');





/*<------------------------------------------------------------------------------------------------------------>*/

//DELETE
/*<< Produtos >>*/
$this->delete('/produtos/delete', 'ProdutosController@delete');

/*<< Clientes >>*/
$this->delete('/clientes/delete', 'ClientesController@delete');

/*<< Notas >>*/
$this->delete('/notas/delete', 'NotasController@delete');

