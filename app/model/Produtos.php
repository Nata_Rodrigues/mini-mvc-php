<?php

namespace app\model;

use app\core\Model;
use PDOException;

class Produtos extends Model
{

	protected $db;
	protected $tablename = 'produtos';

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getId($id)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getAll()
	{
		try {
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function insert($descricao, $valor, $estoque)
	{
		try {
			$array = array();

			$sql = "INSERT INTO $this->tablename SET descricao = :descricao, valor = :valor, estoque = :estoque";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':descricao', $descricao);
			$sql->bindValue(':valor', $valor);
			$sql->bindValue(':estoque', $estoque);

			$sql->execute();

			$sql = "select LAST_INSERT_ID() LASTID";
			$sql = $this->db->prepare($sql);
			$sql->execute();

			if ($sql->rowCount() > 0) {
				$array = $sql->fetch(\PDO::FETCH_ASSOC);
				return $array['LASTID'];
			}

			return $array;
		} catch (PDOException $e) {
			return false;
		}
	}

	public function delete($id)
	{
		try {
			$sql = "DELETE FROM $this->tablename WHERE id = :id	";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if (!$this->getId($id)) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function update($id, $descricao, $valor, $estoque)
	{
		try {
			$array = array();

			$sql = "UPDATE $this->tablename SET descricao = :descricao, valor = :valor, estoque = :estoque WHERE id = :id;";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':descricao', $descricao);
			$sql->bindValue(':valor', $valor);
			$sql->bindValue(':estoque', $estoque);
			$sql->bindValue(':id', $id);
			$sql->execute();

			return true;
		} catch (PDOException $e) {
			return false;
		}
	}
}
