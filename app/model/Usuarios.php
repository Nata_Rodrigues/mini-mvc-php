<?php

namespace app\model;

use app\core\Model;
use PDOException;

class Usuarios extends Model
{

	protected $db;
	protected $tablename = 'usuarios';

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getId($id)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getAll()
	{
		try {
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function insert($nome, $email, $password)
	{

		try {
			$array = array();

			$sql = "INSERT INTO $this->tablename SET nome = :nome, email = :email, password = :password";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':email', $email);
			$sql->bindValue(':password', $password);
			$sql->bindValue(':nome', $nome);

			$sql->execute();

			$sql = "select LAST_INSERT_ID() LASTID";
			$sql = $this->db->prepare($sql);
			$sql->execute();

			if ($sql->rowCount() > 0) {
				$array = $sql->fetch(\PDO::FETCH_ASSOC);
				return $array['LASTID'];
			}

			return $array;
		} catch (PDOException $e) {
			return false;
		}
	}
}
