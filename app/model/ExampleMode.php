<?php

namespace app\model;

use app\core\Model;
use PDOException;

class Usuarios extends Model
{

	protected $db;
	protected $tablename = 'example';

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getId($id)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getAll()
	{
		try {
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}


}
