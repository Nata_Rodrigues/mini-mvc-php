<?php
namespace app\model;
use app\core\Model;
use PDOException;

class ItemNotas extends Model {

	protected $db;
	protected $tablename = 'item_nota';
	
	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value){
		try{
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}

    public function getId($id){
		try{
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}

    public function getAll(){
		try{
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}

    public function getValorTotal($idNota){
		try{
			$sql = "SELECT SUM( valor) as valorTotal FROM $this->tablename WHERE id_nota = :idNota ";
			$stmt = $this->db->prepare($sql);
            $stmt->bindValue(':idNota', $idNota);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}

	public function insert($idNota, $idProduto, $valor, $quantidade)
	{
		try {
			$array = array();

			$sql = "INSERT INTO $this->tablename SET id_nota = :idNota, id_produto = :idProduto, valor = :valor, quantidade = :quantidade";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':idNota', $idNota);
			$sql->bindValue(':idProduto', $idProduto);
			$sql->bindValue(':valor', $valor);
			$sql->bindValue(':quantidade', $quantidade);
			$sql->execute();

			$sql = "select LAST_INSERT_ID() LASTID";
			$sql = $this->db->prepare($sql);
			$sql->execute();

			if ($sql->rowCount() > 0) {
				$array = $sql->fetch(\PDO::FETCH_ASSOC);
				return $array['LASTID'];
			}

			return $array;
		} catch (PDOException $e) {
			return false;
		}
	}

	public function deleteId($id)
	{
		try {
			$sql = "DELETE FROM $this->tablename WHERE id = :id	";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if (!$this->getId($id)) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function deleteNota($id)
	{
		try {
			$sql = "DELETE FROM $this->tablename WHERE id_nota = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if (!$this->getId($id)) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getProdutos_Nota($idNota,$idProduto){
		try{
			$sql = "SELECT * FROM $this->tablename WHERE id_produto = :id_produto AND id_nota = :id_nota";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id_produto', $idProduto);
			$stmt->bindValue(':id_nota', $idNota);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}


}