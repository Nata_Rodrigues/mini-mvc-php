<?php

namespace app\model;

use app\core\Model;
use PDOException;

class Notas extends Model
{

	protected $db;
	protected $tablename = 'notas';

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getId($id)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getAll()
	{
		try {
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function insert($idCliente, $codStatus, $pagamento, $idUsuario)
	{
		try {
			$array = array();

			$sql = "INSERT INTO $this->tablename SET id_cliente = :idCliente, id_status = :codStatus, dt_pagamento = :pagamento, id_usuario= :idUsuario";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':idCliente', $idCliente);
			$sql->bindValue(':codStatus', $codStatus);
			$sql->bindValue(':pagamento', $pagamento);
			$sql->bindValue(':idUsuario', $idUsuario);

			$sql->execute();

			$sql = "select LAST_INSERT_ID() LASTID";
			$sql = $this->db->prepare($sql);
			$sql->execute();

			if ($sql->rowCount() > 0) {
				$array = $sql->fetch(\PDO::FETCH_ASSOC);
				return $array['LASTID'];
			}

			return false;
		} catch (PDOException $e) {
			return false;
		}
	}

	public function delete($id)
	{
		try {
			$sql = "DELETE FROM $this->tablename WHERE id =:id	";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if (!$this->getId($id)) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function update($id_nota, $idCliente, $codStatus, $pagamento, $idUsuario)
	{
		try {
			$array = array();

			$sql = "UPDATE $this->tablename SET id_cliente = :idCliente, id_status = :codStatus, dt_pagamento = :pagamento, id_usuario= :idUsuario WHERE id = :id;";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':idCliente', $idCliente);
			$sql->bindValue(':codStatus', $codStatus);
			$sql->bindValue(':pagamento', $pagamento);
			$sql->bindValue(':idUsuario', $idUsuario);
			$sql->bindValue(':id', $id_nota);
			$sql->execute();

			return true;
		} catch (PDOException $e) {
			return false;
		}
	}

	public function pesquisa($dados)
	{
		try {
			$sql = "SELECT n.* FROM $this->tablename n INNER JOIN clientes c ON n.id_cliente = c.id WHERE 1 ";
			$valida = true;
			if (isset($dados['TP_PESQUISA']) && $dados['TP_PESQUISA'] != '0' && $dados['TP_PESQUISA'] != "") {
				$tpPesquisa = $dados['TP_PESQUISA'];
				$tpPesquisaInput = $dados['TP_PESQUISA_INPUT'];
				switch ($tpPesquisa) {
					case 'cliente':
						$sql = $sql . " AND c.nome LIKE '%$tpPesquisaInput%'";
						break;
					case 'nota':
						$sql = $sql . " AND n.id = '$tpPesquisaInput'";
						break;
				}
				$valida = false;
			}

			if (isset($dados['DATA_INICIAL']) && $dados['DATA_INICIAL'] != '') { // Filtro por Data

				$dataInicial = $dados['DATA_INICIAL'];
				$sql = $sql . " AND n.creat_at >= '$dataInicial'";
				$valida = false;
			}

			if (isset($dados['DATA_FINAL']) && $dados['DATA_FINAL'] != '') { // Filtro por Data

				$dataFinal = $dados['DATA_FINAL'];
				$sql = $sql . " AND n.creat_at <= '$dataFinal'";
				$valida = false;
			}

			if ($valida) {  // Filtra pelo mes atual caso não tenha outros filtros
				$mesAtual = date('m');
				$sql = $sql . " AND MONTH(n.creat_at) = '$mesAtual'";
			}
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function updateColum( $id_nota, $coluna, $value )
	{
		try {
			$array = array();

			$sql = "UPDATE $this->tablename SET $coluna = :value WHERE id = :id";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':value', $value);

			$sql->bindValue(':id', $id_nota);
			$sql->execute();

			return true;
		} catch (PDOException $e) {
			return false;
		}
	}
}
