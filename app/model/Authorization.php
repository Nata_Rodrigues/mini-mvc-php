<?php
namespace app\model;
use app\core\Model;
use PDOException;

class Authorization extends Model {

	protected $db;
	protected $tablename = 'usuarios';

	public function __construct() {
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function Login($email, $password){
		try{
			$sql = "SELECT * FROM $this->tablename WHERE email = :email AND password = :password";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':email', $email);
			$stmt->bindValue(':password', $password);
			$stmt->execute();
			if($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		}catch (PDOException $e){
			echo $e->getMessage();exit;
		}
	}
 
}