<?php

namespace app\model;

use app\core\Model;
use PDOException;

class Payment extends Model
{

	protected $db;
	protected $tablename = 'payment_mercado_pago';

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public static function getDataBase()
	{
		global $db;
		return $db;
	}

	public function findBy($column, $value)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE $column = :value";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':value', $value);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getId($id)
	{
		try {
			$sql = "SELECT * FROM $this->tablename WHERE id = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}

	public function getAll()
	{
		try {
			$sql = "SELECT * FROM $this->tablename";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			if ($stmt->rowCount() > 0) {
				return $stmt->fetch(\PDO::FETCH_ASSOC);
			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit;
		}
	}


	public function insert($data)
	{
		try {
			$array = array();

			$sql = "INSERT INTO $this->tablename SET  collection_id = :collection_id, collection_status = :collection_status, payment_id = :payment_id, status = :status , merchant_order_id = :merchant_order_id, external_reference =:external_reference , preference_id = :preference_id, processing_mode = :processing_mode,  payment_type= :payment_type  ";
			$sql = $this->db->prepare($sql);
			$sql->bindValue(':collection_id', $data['collection_id']);
			$sql->bindValue(':collection_status', $data['collection_status']);
			$sql->bindValue(':payment_id', $data['payment_id']);
			$sql->bindValue(':status', $data['status']);
			$sql->bindValue(':merchant_order_id', $data['merchant_order_id']);
			$sql->bindValue(':external_reference', $data['external_reference']);
			$sql->bindValue(':preference_id', $data['preference_id']);
			$sql->bindValue(':processing_mode', $data['processing_mode']);
			$sql->bindValue(':payment_type', $data['payment_type']);
			$sql->execute();

			$sql = "select LAST_INSERT_ID() LASTID";
			$sql = $this->db->prepare($sql);
			$sql->execute();

			if ($sql->rowCount() > 0) {
				$array = $sql->fetch(\PDO::FETCH_ASSOC);
				return $array['LASTID'];
			}

			return $array;
		} catch (PDOException $e) {
			return false;
		}
	}
}
