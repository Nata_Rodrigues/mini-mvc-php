<?php

if (empty($_GET['DEBUG'])) {
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
    error_reporting(-1);
}
require_once('../vendor/autoload.php');
require_once('../app/config/config.php');
require_once('../app/core/Functions.php');
try {
    require_once('../app/DataBase/config.php');
} catch (Exception $e) {
    header("HTTP/1.0 500");
    $response = array(
        'message' => 'Erro de configuração: config.php não está presente'
    );
    die(json_encode($response));
}
(new \app\core\RouterCore());
