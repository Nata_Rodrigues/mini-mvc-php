$(document).ready(function () {



    loadTable();
    $('#btn_adicionar').on('click', function () {
        limpaModal();
        $('#exampleModal').modal('show');
        $('#titleModalLabel').text('Adicionar Produto');
       

    });
    $('#mytable').on('click', "#editar", function () {
        limpaModal();
        var row = $(this).parents('tr');
        var codProduto = row.find('#codProduto').html();
        getProduto(codProduto);

    });

    $('#mytable').on('click', "#excluir", function () {
        var row = $(this).parents('tr');
        var codProduto = row.find('#codProduto').html();
        var descricao = row.find('#descricao').html();

        Swal.fire({
            title: 'Atenção!',
            text: "O produto " + descricao + " será excluído, deseja confirmar?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim'
        }).then((result) => {
            if (result.isConfirmed) {
                var payload = JSON.stringify({
                    ID: codProduto,
                });
                deleteProduto(payload);
            }
        })

    });

    $('#search').on('keyup', function () {
        filter();
    });

    $('#salvar_produto').on('click', function () {

        var descricaoProduto = $('#descricao_produto');
        var valorProduto = $('#valor_produto');
        var estoqueProduto = $('#estoque_produto');
        var idProduto = $('#id_produto');

        const camposObg = [
            descricaoProduto,
            valorProduto,
            estoqueProduto,
        ];

        if (validaCampos(camposObg)) {

            var payload = JSON.stringify({
                ID:idProduto.val(),
                DESCRICAO: descricaoProduto.val(),
                VALOR: valorProduto.val(),
                ESTOQUE: estoqueProduto.val(),
            });
            salvar(payload);

        }



    });

    $("#valor_produto").mask('#.##0,00', {
        reverse: true
    });

});

function loadTable() {
    $('#spinner_table').toggleClass("spinner-border spinner-border-sm"); // Liga spiner

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("produtos/getAll", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_table').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            if (result) {
                data = result;
                paginate(data);
            } else {
                $("#mytable tbody tr").remove();
                let row = "<tr><td colspan='5' style='text-align: center;'>Sem Registro...</td></tr>";
                $("#mytable tbody").append(row);
            }
        })
        .catch(error => console.log('error', error));
}

function paginate(dados) { // Funcção que monta a tabela e a paginação


    const state = {
        page: 1,
        perPage() {
            return document.querySelector('.perPage').value
        },
        totalPage() {
            return Math.ceil(dados.length / state.perPage())

        },
        maxVisibleButtons: 5
    }

    const list = {
        create(item) {
            var table = "";

            table = table.concat("<tr charset='ISO-8859-1'>",
                "<th style='text-align: center;' id='codProduto' scope='row'>" + item.id + "</th>",
                "<td style='text-align: center;' id='descricao'>" + item.descricao + "</td>",
                "<td style='text-align: right;'>" + formataValor(item.valor) + "</td>",
                "<td style='text-align: center;'>" + item.estoque + "</td>",
                "<td style='text-align: right;'>",
                "<div class='d-flex gap-1' style='text-align: rigth; display: flex; justify-content: flex-end;'>",
                "<a class='labelicon ' style='background-color: #378dfc;' id='editar' name='editar' title='Editar'><i class='fi fi-sr-pencil'></i></a>",
                "<a class='labelicon ' style='background-color: #dc3545;' id='excluir' name='excluir' title='Excluir'><i class='fi fi-sr-trash'></i></a>",
                "</div>",
                "</td>",
                "</tr>"
            );
            $("#mytable tbody").append(table);

        },
        update() {
            $("#mytable tbody tr").remove();
            let page = state.page - 1
            let start = page * state.perPage()
            let end = parseInt(start) + parseInt(state.perPage())

            const paginatedItems = dados.slice(start, end)
            paginatedItems.forEach(list.create)
        }
    }

    const html = {
        get(element) {
            return document.querySelector(element)
        }
    }

    const controls = {
        perPage(number) {
            if (perPage != number) {
                perPage = number;
            }



        },
        next() {
            const lastPage = state.page < state.totalPage()
            if (lastPage) {
                state.page++
            }
        },
        prev() {

            const firstPage = state.page > 1;
            if (firstPage) {
                state.page--
            }
        },
        goTo(page) {
            if (page < 1) {
                page = 1
            }
            state.page = +page
            if (page > state.totalPage()) {
                state.page = state.totalPage()
            }

        },
        createListeners() {
            html.get('.first').addEventListener('click', () => {
                controls.goTo(1);
                update();
            })
            html.get('.last').addEventListener('click', () => {
                controls.goTo(state.totalPage());
                update();

            })
            html.get('.prev').addEventListener('click', () => {
                controls.prev();
                update();

            })
            html.get('.next').addEventListener('click', () => {
                controls.next();
                update();

            })

            html.get('.perPage').addEventListener('change', () => {
                let number = html.get('.perPage').value
                controls.perPage(number)
                update()

            })

        }
    }



    const buttons = {

        element: html.get('.numbers'),
        create(number) {
            const button = document.createElement('a')
            button.classList.add('page-link')
            button.innerHTML = number
            if (state.page == number) {
                button.classList.add('active')
            }
            button.addEventListener('click', (event) => {
                const page = event.target.innerText
                controls.goTo(page)
                update()
            })
            buttons.element.appendChild(button)
        },
        update() {
            buttons.element.innerHTML = ""
            const {
                maxLeft,
                maxRight
            } = buttons.calculateMaxVisible()
            for (let page = maxLeft; page <= maxRight; page++) {
                buttons.create(page)
            }

        },
        calculateMaxVisible() {
            const {
                maxVisibleButtons
            } = state
            let maxLeft = (state.page - Math.floor(maxVisibleButtons / 2))
            let maxRight = (state.page + Math.floor(maxVisibleButtons / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = maxVisibleButtons
            }
            if (maxRight > state.totalPage()) {
                maxLeft = state.totalPage() - (maxVisibleButtons - 1)
                maxRight = state.totalPage()
                if (maxLeft < 1) {
                    maxLeft = 1
                }
            }

            return {
                maxLeft,
                maxRight
            }
        }
    }

    function update() {
        list.update()
        buttons.update()
    }

    function init() {
        update()
        controls.createListeners()
    }

    init();
}


function formatarData(date) { // função para formatar a data
    let dataFormatada = '';
    if (date.length > 1) {
        dataFormatada = new Intl.DateTimeFormat('pt-BR', {
            dateStyle: 'full',
            timeStyle: 'long'
        }).format(

            new Date(date)

        )

    }

    return dataFormatada;

}

function filter() { // Função para input de pesquisa rápida

    /**
     * Filter array items based on search criteria (query)
     */
    function filterItems(arr, query) {
        let result = []
        let teste = arr.filter((el) => {

            Object.keys(el).forEach((key) => {
                if (el[key]) {
                    if (el[key].toLowerCase().includes(query.toLowerCase())) {
                        result.push(el)

                    }

                }
            })


        }


        );
        return result

    }
    let search = document.getElementById('search').value;
    if (search.length >= 3) {

        let result = filterItems(data, search);

        if (result.length > 0) {
            paginate(result)
        } else {
            paginate(data)
        }

    } else {
        paginate(data)
    }


}


function validaCampos(campos) {

    let camposVazios = campos.filter((d) => {

        let valor = d.val();
        if (valor == '' || (parseFloat(valor) < 0)) {
            d.css("border", "1px solid red").addClass("is-invalid");
            var form = d.parents('div.form-group');
            form.find('small').text('Preecha os campos destacados com valores válidos.');
            return d
        } else {
            d.css("border", "").removeClass("is-invalid");
            var form = d.parents('div.form-group');
            form.find('small').text('');
        }
    });
    if (camposVazios.length > 0) {
        return false
    }
    return true;
}

function salvar(payload) {

    $('#spinner_save').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#salvar_produto').addClass("disabled");

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("produtos/salvar", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_save').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#salvar_produto').removeClass("disabled");

            if (result.status) {
                limpaModal();
                $('#exampleModal').modal('hide');
                loadTable();
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });


            } else {
                retorno = result.status;
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        }).catch(error => console.log('error', error));
}

function deleteProduto(payload) {


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("produtos/delete", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            if (result.status) {
                loadTable();
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            } else {
                retorno = result.status;
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        }).catch(error => console.log('error', error));

}

function getProduto(id) {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("produtos/getId/?ID=" + id, requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            if (result) {
                preencheModal(result);
            } else {
                return false;
            }
        })
        .catch(error => console.log('error', error));
}

function limpaModal() {

    $('#descricao_produto').val('').removeClass("is-invalid").css("border", "");
    $('#valor_produto').val('').removeClass("is-invalid").css("border", "");
    $('#estoque_produto').val('').removeClass("is-invalid").css("border", "");
    $('#id_produto').val('');
     $('#modal_body').find('small').text('');;


}

function preencheModal(dados) {

    $('#descricao_produto').val(dados.descricao);
    $('#valor_produto').val(dados.valor);
    $('#estoque_produto').val(dados.estoque);
    $('#id_produto').val(dados.id);
    $('#exampleModal').modal('show');
    $('#titleModalLabel').text('Editar Produto');


}

