
$(document).ready(function () {
    initalizeMecadoPago();

});

// const mercadopago = new MercadoPago("APP_USR-140a6a03-ead2-4d75-8604-7d0da3ec5923", {
//     locale: 'pt-BR' // The most common are: 'c', 'es-AR' and 'en-US'
// });

const mercadopago = new MercadoPago("TEST-407178d7-8601-4656-967f-4e22178162c7", {
    locale: 'pt-BR' // The most common are: 'c', 'es-AR' and 'en-US'
});

function createCheckoutButton(preferenceId) {
    // Initialize the checkout
    mercadopago.checkout({
        preference: {
            id: preferenceId
        },
        render: {
            container: '#button-checkout', // Class name where the payment button will be displayed
            label: 'Pagar', // Change the payment button text (optional)
        }
    });
}


function initalizeMecadoPago() {
    const publicKey = $('#public_key').val();

    const orderData = {
        ID: $('#id_nota').val()
    };

    fetch("payment/save", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(orderData),
    }).then(function (response) {
        return response.json();
    }).then(function (preference) {
            
            createCheckoutButton(preference.id);


        })
        .catch(function (d) {
            alert(d);
        });
}



