$(document).ready(function () {

    loadTable()


    $('#mytable').on('click', "#imprimir", function () {

        var row = $(this).parents('tr');
        var idNota = row.find('#idNota').html();
        var url_atual = window.location.href;
        window.open(url_atual + "/comprovante?ID=" + idNota, "_blank");
    });

    $('#mytable').on('click', "#envia_cobranca", function () {

        var row = $(this).parents('tr');
        var idNota = row.find('#idNota').html();
        $("#id_nota_cobranca").val(idNota);
        let payload = JSON.stringify({
            'ID': idNota
        });
        envia_cobranca(payload);

    });
    $('#btn_pesquisa').on('click', function () {

        loadTable();

    });

    $('#btn_adicionar').on('click', function () {
        limpaModal();
        $('#exampleModal').modal('show');
        $('#titleModalLabel').text('Nova Nota');


    });
    $('#mytable').on('click', "#editar", function () {
        limpaModal();
        var row = $(this).parents('tr');
        var idNota = row.find('#idNota').html();
        getNota(idNota);

    });

    $('#mytable').on('click', "#excluir", function () {
        var row = $(this).parents('tr');
        var idNota = row.find('#idNota').html();

        Swal.fire({
            title: 'Atenção!',
            text: "A Nota " + idNota + " será excluída, deseja confirmar?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim'
        }).then((result) => {
            if (result.isConfirmed) {
                var payload = JSON.stringify({
                    ID: idNota,
                });
                deleteNota(payload);
            }
        })

    });

    $('#produtos_notas').on('click', "#excluir_produto", function () {
        var row = $(this).parents('tr');
        var indexProduto = row.find('#index_produto').val();
        deleteProdutos(indexProduto);


    });
    $('#search').on('keyup', function () {
        filter();
    });

    $('#salvar_nota').on('click', function () {


        var idCliente = $('#select_cliente');
        var idNota = $('#id_nota');
        var status = $('#status_nota');
        var pagamento = $('#dt_pagamento');

        const camposObg = [
            idCliente,
            status
        ];

        if (validaCampos(camposObg) && validaProdutos() && validaStatus()) {



            var payload = JSON.stringify({
                ID: idNota.val(),
                ID_CLIENTE: idCliente.val(),
                PRODUTOS: returnProdutos(),
                STATUS: status.val(),
                PAGAMENO: pagamento.val()
            });

            salvar(payload);
        }



    });

    $('#select_produto').on('change', function () {
        let idProduto = $('#select_produto').val()
        preencheValor(idProduto);
    });

    $('#quantidade_produto').on('keyup', function () {
        let quantidade = $('#quantidade_produto').val();
        calculaValor(quantidade);

    });

    $('#btn_adicionar_produto').on('click', function () {
        let idProduto = $('#select_produto');
        let descricao = $('#select_produto :selected');
        let quantidade = $('#quantidade_produto');
        let valor = $('#valor_produto');


        const camposObg = [
            quantidade,
            valor,
            idProduto
        ];

        if (validaCampos(camposObg)) {
            let dados = {
                'id': idProduto.val(),
                'descricao': descricao.text(),
                'quantidade': quantidade.val(),
                'valor': valor.val()
            };
            loadTableProdutos(dados);
        }
    });

    $(".valor_produto").mask('#.##0,00', {
        reverse: true
    });

    $(".select2").select2({
        dropdownParent: $("#exampleModal")
    });;

    $('#enviar_email_coanca').on('click', function () {

       
        var idNota = $("#id_nota_cobranca").val();
        let payload = JSON.stringify({
            'ID': idNota
        });

        envia_email_cobranca(payload);



    });


});

function loadTable() {
    spinner_pesquisa
    $('#spinner_pesquisa').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#spinner_table').toggleClass("spinner-border spinner-border-sm"); // Liga spiner

    var payload = JSON.stringify({
        TP_PESQUISA: $('#tp_pesquisa').val(),
        TP_PESQUISA_INPUT: $('#tp_pesquisa_input').val(),
        DATA_INICIAL: $('#data_inicial').val(),
        DATA_FINAL: $('#data_final').val(),
    });

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };

    fetch("notas/pesquisa", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_table').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#spinner_pesquisa').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            if (result) {
                data = result;
                paginate(data);
            } else {
                $("#mytable tbody tr").remove();
                let row = "<tr><td colspan='5' style='text-align: center;'>Sem Registro...</td></tr>";
                $("#mytable tbody").append(row);
            }
        })
        .catch(error => console.log('error', error));
}

function paginate(dados) { // Funcção que monta a tabela e a paginação


    const state = {
        page: 1,
        perPage() {
            return document.querySelector('.perPage').value
        },
        totalPage() {
            return Math.ceil(dados.length / state.perPage())

        },
        maxVisibleButtons: 5
    }

    const list = {
        create(item) {
            var table = "";

            table = table.concat("<tr charset='ISO-8859-1'>",
                "<th style='text-align: center;' id='idNota' scope='row'>" + item.id + "</th>",
                "<td style='text-align: center;' id='data'>" + moment(item.creat_at).format("DD/MM/YYYY") + "</td>",
                "<td style='text-align: center;' id='nomeCliente'>" + item.cliente + "</td>",
                "<td style='text-align: center;'><div class='status' style='background-color: " + item.cor_status + ";'>" + item.status + "</div></td>",
                "<td style='text-align: center;'>" + formataValor(item.valorTotal) + "</td>",
                "<td style='text-align: right;'>",
                "<div class='d-flex gap-1' style='text-align: rigth; display: flex; justify-content: flex-end;'>",
                "<a class='labelicon ' style='background-color: #6d6969;' id='imprimir' name='Imprimir' title='Imprimir'><i class='fi fi-rr-document'></i></a>",
                "<a class='labelicon ' style='background-color: #a807f2;' id='envia_cobranca' name='cobranca' title='Enviar Cobrança'><i class='fi fi-bs-paper-plane'></i></a>",
                "<a class='labelicon ' style='background-color: #378dfc;' id='editar' name='editar' title='Editar'><i class='fi fi-sr-pencil'></i></a>",
                "<a class='labelicon ' style='background-color: #dc3545;' id='excluir' name='excluir' title='Excluir'><i class='fi fi-sr-trash'></i></a>",
                "</div>",
                "</td>",
                "</tr>"
            );
            $("#mytable tbody").append(table);

        },
        update() {
            $("#mytable tbody tr").remove();
            let page = state.page - 1
            let start = page * state.perPage()
            let end = parseInt(start) + parseInt(state.perPage())

            const paginatedItems = dados.slice(start, end)
            paginatedItems.forEach(list.create)
        }
    }

    const html = {
        get(element) {
            return document.querySelector(element)
        }
    }

    const controls = {
        perPage(number) {
            if (perPage != number) {
                perPage = number;
            }



        },
        next() {
            const lastPage = state.page < state.totalPage()
            if (lastPage) {
                state.page++
            }
        },
        prev() {

            const firstPage = state.page > 1;
            if (firstPage) {
                state.page--
            }
        },
        goTo(page) {
            if (page < 1) {
                page = 1
            }
            state.page = +page
            if (page > state.totalPage()) {
                state.page = state.totalPage()
            }

        },
        createListeners() {
            html.get('.first').addEventListener('click', () => {
                controls.goTo(1);
                update();
            })
            html.get('.last').addEventListener('click', () => {
                controls.goTo(state.totalPage());
                update();

            })
            html.get('.prev').addEventListener('click', () => {
                controls.prev();
                update();

            })
            html.get('.next').addEventListener('click', () => {
                controls.next();
                update();

            })

            html.get('.perPage').addEventListener('change', () => {
                let number = html.get('.perPage').value
                controls.perPage(number)
                update()

            })

        }
    }



    const buttons = {

        element: html.get('.numbers'),
        create(number) {
            const button = document.createElement('a')
            button.classList.add('page-link')
            button.innerHTML = number
            if (state.page == number) {
                button.classList.add('active')
            }
            button.addEventListener('click', (event) => {
                const page = event.target.innerText
                controls.goTo(page)
                update()
            })
            buttons.element.appendChild(button)
        },
        update() {
            buttons.element.innerHTML = ""
            const {
                maxLeft,
                maxRight
            } = buttons.calculateMaxVisible()
            for (let page = maxLeft; page <= maxRight; page++) {
                buttons.create(page)
            }

        },
        calculateMaxVisible() {
            const {
                maxVisibleButtons
            } = state
            let maxLeft = (state.page - Math.floor(maxVisibleButtons / 2))
            let maxRight = (state.page + Math.floor(maxVisibleButtons / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = maxVisibleButtons
            }
            if (maxRight > state.totalPage()) {
                maxLeft = state.totalPage() - (maxVisibleButtons - 1)
                maxRight = state.totalPage()
                if (maxLeft < 1) {
                    maxLeft = 1
                }
            }

            return {
                maxLeft,
                maxRight
            }
        }
    }

    function update() {
        list.update()
        buttons.update()
    }

    function init() {
        update()
        controls.createListeners()
    }

    init();
}


function formatarData(date) { // função para formatar a data
    let dataFormatada = '';
    if (date.length > 1) {
        dataFormatada = new Intl.DateTimeFormat('pt-BR', {
            dateStyle: 'full',
            timeStyle: 'short'
        }).format(

            new Date(date)

        )

    }

    return dataFormatada;

}

function filter() { // Função para input de pesquisa rápida

    /**
     * Filter array items based on search criteria (query)
     */
    function filterItems(arr, query) {
        let result = []
        let teste = arr.filter((el) => {

            Object.keys(el).forEach((key) => {
                if (el[key]) {
                    if (el[key].toLowerCase().includes(query.toLowerCase())) {
                        result.push(el)

                    }

                }
            })


        }


        );
        return result

    }
    let search = document.getElementById('search').value;
    if (search.length >= 3) {



        let result = filterItems(data, search);

        if (result.length > 0) {
            paginate(result)
        } else {
            paginate(data)
        }

    } else {
        paginate(data)
    }


}


function validaCampos(campos) {


    let camposVazios = campos.filter((d) => {

        let valor = d.val();
        if (valor == '' || (parseFloat(valor) < 0)) {
            d.css("border", "1px solid red").addClass("is-invalid");
            var form = d.parents('div.modal_cliente');
            form.find('small').text('Selecione um cliente para continuar.');


            return d
        } else {
            d.css("border", "").removeClass("is-invalid");
            var form = d.parents('div.modal_cliente');
            form.find('small').text('');

        }
    });
    if (camposVazios.length > 0) {
        toasts('Preencha os campos com dados válidos!', 'danger');
        return false
    }
    return true;
}

function salvar(payload) {

    $('#spinner_save').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#salvar_nota').addClass("disabled");

    salvar_nota
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("notas/salvar", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_save').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#salvar_nota').removeClass("disabled");

            if (result.status) {
                limpaModal();
                $('#exampleModal').modal('hide');
                loadTable();
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });


            } else {
                retorno = result.status;
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        }).catch(error => console.log('error', error));
}

function deleteNota(payload) {


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("notas/delete", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            if (result.status) {
                loadTable();
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            } else {
                retorno = result.status;
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        }).catch(error => console.log('error', error));

}

function getNota(id) {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("notas/getId/?ID=" + id, requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            if (result) {
                preencheModal(result);
            } else {
                return false;
            }
        })
        .catch(error => console.log('error', error));
}

function limpaModal() {

    $('#quantidade_produto').val('').removeClass("is-invalid").css("border", "");
    $('#valor_produto').val('').removeClass("is-invalid").css("border", "");
    $("#produtos_notas tbody tr").remove();
    $('#id_nota').val('');
    $('#modal_body').find('small').text('');
    $('#select_produto').val('-1');
    $('#select_cliente').val([-1]);
    $('#select_cliente').trigger('change');
    $('#status_nota').val('-1');
    $('#dt_pagamento').val('');
    $('#total_nota').html(formataValor(0));
    dataProdutos = [];

}

function preencheModal(dados) {
    $('#id_nota').val(dados.id);
    $('#select_produto').val('-1');
    $('#select_cliente').val([dados.id_cliente]);
    $('#select_cliente').trigger('change');
    $('#status_nota').val(dados.id_status);
    $('#dt_pagamento').val(dados.dt_pagamento);

    let produtos = dados.produtos;
    produtos.forEach((d) => {
        let dados = {
            'id': d.id_produto,
            'descricao': d.descricao,
            'quantidade': d.quantidade,
            'valor': d.valor
        };
        loadTableProdutos(dados);


    });

    $('#exampleModal').modal('show');
    $('#titleModalLabel').text('Editar Nota');
}

function preencheValor(id) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("produtos/getId/?ID=" + id, requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            if (result) {
                valorProduto = result.valor;
                $('#valor_produto').val(result.valor);
            } else {
                return false;
            }
        })
        .catch(error => console.log('error', error));
}

function calculaValor(quantidade) {
    let valor = parseFloat(quantidade * valorProduto);
    $('#valor_produto').val(valor);
}


function loadTableProdutos(dados) {

    if (typeof dataProdutos == 'undefined') {

        dataProdutos = [dados];
    } else {

        dataProdutos.push(dados);
    }
    paginateProdutos(dataProdutos);
    $('#quantidade_produto').val('');
    $('#valor_produto').val('');
    $('#select_produto').val('-1');

}

function paginateProdutos(dados) { // Funcção que monta a tabela e a paginação


    const state = {
        page: 1,
        perPage() {
            return 5
        },
        totalPage() {
            return Math.ceil(dados.length / state.perPage())

        },
        maxVisibleButtons: 5
    }

    const list = {
        create(item, index) {
            var table = "";

            table = table.concat("<tr charset='ISO-8859-1'>",
                "<input type='hidden' id='index_produto' value='" + index + "'/>",
                "<th style='text-align: center;' id='codCliente' scope='row'>" + item.id + "</th>",
                "<td style='text-align: center;' id='nomeCliente'>" + item.descricao + "</td>",
                "<td style='text-align: center;'>" + item.quantidade + "</td>",
                "<td style='text-align: center;'>" + formataValor(item.valor) + "</td>",
                "<td style='text-align: right;'>",
                "<div class='d-flex gap-1' style='text-align: rigth; display: flex; justify-content: flex-end;'>",
                "<a class='labelicon ' style='background-color: #dc3545;' id='excluir_produto' name='excluir_produto' title='Excluir'><i class='fi fi-sr-trash'></i></a>",
                "</div>",
                "</td>",
                "</tr>"
            );
            $("#produtos_notas tbody").append(table);

        },
        update() {
            $("#produtos_notas tbody tr").remove();
            let page = state.page - 1
            let start = page * state.perPage()
            let end = parseInt(start) + parseInt(state.perPage())

            const paginatedItems = dados.slice(start, end)
            paginatedItems.forEach(list.create)
        }
    }

    const html = {
        get(element) {
            return document.querySelector(element)
        }
    }

    const controls = {
        perPage(number) {
            if (perPage != number) {
                perPage = number;
            }



        },
        next() {
            const lastPage = state.page < state.totalPage()
            if (lastPage) {
                state.page++
            }
        },
        prev() {

            const firstPage = state.page > 1;
            if (firstPage) {
                state.page--
            }
        },
        goTo(page) {
            if (page < 1) {
                page = 1
            }
            state.page = +page
            if (page > state.totalPage()) {
                state.page = state.totalPage()
            }

        },
        createListeners() {
            html.get('.first-produto').addEventListener('click', () => {
                controls.goTo(1);
                update();
            })
            html.get('.last-produto').addEventListener('click', () => {
                controls.goTo(state.totalPage());
                update();

            })
            html.get('.prev-produto').addEventListener('click', () => {
                controls.prev();
                update();

            })
            html.get('.next-produto').addEventListener('click', () => {
                controls.next();
                update();

            })


        }
    }



    const buttons = {

        element: html.get('.numbers-produto'),
        create(number) {
            const button = document.createElement('a')
            button.classList.add('page-link')
            button.innerHTML = number
            if (state.page == number) {
                button.classList.add('active')
            }
            button.addEventListener('click', (event) => {
                const page = event.target.innerText
                controls.goTo(page)
                update()
            })
            buttons.element.appendChild(button)
        },
        update() {
            buttons.element.innerHTML = ""
            const {
                maxLeft,
                maxRight
            } = buttons.calculateMaxVisible()
            for (let page = maxLeft; page <= maxRight; page++) {
                buttons.create(page)
            }

        },
        calculateMaxVisible() {
            const {
                maxVisibleButtons
            } = state
            let maxLeft = (state.page - Math.floor(maxVisibleButtons / 2))
            let maxRight = (state.page + Math.floor(maxVisibleButtons / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = maxVisibleButtons
            }
            if (maxRight > state.totalPage()) {
                maxLeft = state.totalPage() - (maxVisibleButtons - 1)
                maxRight = state.totalPage()
                if (maxLeft < 1) {
                    maxLeft = 1
                }
            }

            return {
                maxLeft,
                maxRight
            }
        }
    }

    function update() {
        list.update()
        buttons.update()
    }

    function init() {
        update()
        controls.createListeners()
        preencheTotal(dados)
    }

    init();
}
function deleteProdutos(index) {
    dataProdutos.splice(index, 1);
    paginateProdutos(dataProdutos);
}

function validaProdutos() {

    if ((typeof dataProdutos == 'undefined') || dataProdutos.length == 0) {
        toasts('Insira no mínimo 1 produto!', 'danger');
        $('#produtos_notas').css("border", "1px solid red");

        return false

    } else {
        $('#produtos_notas').css("border", "");
    }

    return true;
}

function returnProdutos() {
    return dataProdutos;
}

function validaStatus() {

    let status = $('#status_nota').val();
    let pagamento = $('#dt_pagamento').val();
    if (status == 2 && pagamento == '') {
        $('#dt_pagamento').css("border", "1px solid red");
        toasts('Insira a data do pagamento!', 'danger');
        return false;
    } else {
        $('#dt_pagamento').css("border", "");

    }

    return true;
}

function preencheTotal(dados) {

    let valorTotal = 0;
    dados.forEach((d) => {
        let valor = parseFloat(d.valor);
        valorTotal = parseFloat(valorTotal + valor);
    });
    $('#total_nota').html(formataValor(valorTotal));
}

function envia_cobranca(payload) {

    $('#spinner_cobranca').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("payment/getLink", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_cobranca').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            if (result.status) {
                $('#cobrancaModal_body').html("Link: <a href=" + result.link + " style = 'textAlign:center'; target='_blank'>Link de cobrança.</a>");
                $('#enviar_coanca').prop("disabled", false);
                $('#cobrancaModal').modal('show');

            } else {
                $('#enviar_coanca').prop("disabled", true);
                $('#cobrancaModal_body').html(result.message);
                $('#cobrancaModal').modal('show');
            }
        }).catch(error => console.log('error', error));



}

function envia_email_cobranca(payload){

    $('#spinner_cobranca').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#enviar_email_coanca').addClass("disabled");

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: payload,
        redirect: 'follow'
    };
    var retorno = null;
    const response = fetch("notas/enviaCobranca", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner_cobranca').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#enviar_email_coanca').removeClass("disabled");
            if (result.status) {
                toasts(result.message, 'success');

            } else {
                toasts(result.message, 'danger');
            }
        }).catch(error => console.log('error', error));



}