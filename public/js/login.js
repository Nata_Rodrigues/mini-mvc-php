$(document).ready(function () {

    $('#login').on('click', () => {
        
        const user = $('#user');
        const password = $('#password');
        let valida = valida_campo_vazio(user, password);
        valida ? login(user, password) : false;

    });
});

function valida_campo_vazio(user, password) {
    if (!user.val() || !password.val()) {
        !user.val() ? user.addClass("is-invalid") : user.removeClass("is-invalid");
        !password.val() ? password.addClass("is-invalid") : password.removeClass("is-invalid");
        $('#alert1').removeClass('show');
        $('#alert2').addClass('show');

        return false;
    } else {

        $('#alert1').addClass('show');
        user.removeClass("is-invalid");
        password.removeClass("is-invalid");
        return true;
    }
}

function login(user, password) {

    $('#spinner').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#login').toggleClass("disabled");
    const data = JSON.stringify({

        email: user.val(),
        password: password.val()
    });

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: data,
        redirect: 'follow'
    };


    fetch("login", requestOptions)
        .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#login').toggleClass("disabled");

            if (result.status) {
                $('#alert2').addClass('show');
                window.location.href = 'home';


            } else {
                $('#alert2').html(result.message);
                $('#alert2').removeClass('show');
            }
        })
        .catch(error => console.log('error', error));


}