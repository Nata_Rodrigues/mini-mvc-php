$(document).ready(function () {

    $('#logout').on('click', () => {

        logout();
    });
});

function logout() {

    $('#spinner').toggleClass("spinner-border spinner-border-sm"); // Liga spiner
    $('#logout').toggleClass("disabled");

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("logout", requestOptions)
    .then(response => response.text())
        .then((result) => {
            result = JSON.parse(result);
            $('#spinner').toggleClass("spinner-border spinner-border-sm"); //Desliga spiner
            $('#logout').toggleClass("disabled");

            if (result.status) {
                window.location.href = '';


            }
        })
        .catch(error => console.log('error', error));


}