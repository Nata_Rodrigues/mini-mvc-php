function formataValor(valor) {

  let valorFormatado = parseFloat(valor).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
  return valorFormatado;
}

$(document).ready(function () {


  $('.phone-mask').each(function (i, el) {
    $('#' + el.id).mask("(00) 00000-0000");
  })
  function updateMask(event) {
    var $element = $('#' + this.id);
    $(this).off('blur');
    $element.unmask();
    if (this.value.replace(/\D/g, '').length > 10) {
      $element.mask("(00) 00000-0000");
    } else {
      $element.mask("(00) 0000-00009");
    }
    $(this).on('blur', updateMask);
  }

})

function toasts(mensagem, type) {

  $(".toast-container").remove();
  let toast = '';
  toast = toast.concat("<div class='toast-container position-absolute top-0 end-0 p-3'>",
      "<div id='myToast' class='toast align-items-center text-white bg-"+type+" border-0' role='alert' aria-live='assertive' aria-atomic='true'>",
      "<div class='d-flex'><div class='toast-body'>"+mensagem+"</div>",
      "<button type='button' class='btn-close btn-close-white me-2 m-auto' data-bs-dismiss='toast' aria-label='Close'></button></div></div>",
      "</div>"
  );
  $("body").append(toast);
  $("#myToast").toast("show");

}